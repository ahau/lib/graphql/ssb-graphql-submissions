const test = require('tape')
const { isMsgId } = require('ssb-ref')
const { promisify: p } = require('util')

const TestBot = require('../../test-bot')
const { sleep, handleErr, GetSubmission, PROPOSE_EDIT_MUTATION } = require('../../lib/helpers')

const query = `
query ($id: String!) {
  getSubmission (id: $id) {
    ...on SubmissionGroupPerson {
      sourceRecord {
        altNames
      }
      details {
        altNamesSubmission {
          add
          remove
        }
      }
    }
  }
}
`

test('proposeEdit person/group altNames', async (t) => {
  t.plan(7)

  const kaitiaki = await TestBot({ name: 'kaitiaki' })
  const member = await TestBot({ name: 'member' })

  kaitiaki.getSubmission = GetSubmission(kaitiaki, t, query)
  member.getSubmission = GetSubmission(member, t, query)

  t.teardown(() => {
    kaitiaki.close()
    member.close()
  })

  let submission

  // 1. kaitiaki creates a group
  const { groupId } = await p(kaitiaki.ssb.tribes.create)({ addPOBox: true })

  // 2. kaitiaki invites the member to the group
  await p(kaitiaki.ssb.tribes.invite)(groupId, [member.id], {})

  // 3. kaitiaki creates a profile in the group
  const profileDetails = {
    preferredName: 'Cherese',
    legalName: 'Cherese',
    authors: {
      add: ['*']
    },
    altNames: {
      add: ['Reese', 'Oops']
    },
    recps: [groupId]
  }

  const profileId = await p(kaitiaki.ssb.profile.person.group.create)(profileDetails)
  t.true(isMsgId(profileId), 'kaitiaki creates a profile')

  // replicate to the member
  await kaitiaki.replicate(member)
  await member.replicate(kaitiaki)

  // give it a little time to rebuild
  await sleep(500)

  // 4. member creates a submission to edit the profiles altNames
  const res = await member.apollo.mutate({
    mutation: PROPOSE_EDIT_MUTATION,
    variables: {
      profileId,
      input: {
        altNames: {
          add: ['Reesey'],
          remove: ['Oops']
        }
      },
      groupId,
      comment: 'Can you please amend the altNames?'
    }
  })
    .catch(handleErr)

  t.error(res.errors, 'proposeEditGroupPerson profile without error')

  const submissionId = res.data.proposeEditGroupPerson
  t.true(isMsgId(submissionId), 'proposeEditGroupPerson returned msgId for the new submission')

  await member.replicate(kaitiaki)

  // 5. the member can query their submission
  submission = await member.getSubmission(submissionId)

  const expected = {
    sourceRecord: {
      altNames: ['Oops', 'Reese']
    },
    details: {
      altNamesSubmission: {
        add: ['Reesey'],
        remove: ['Oops']
      }
    }
  }

  t.deepEqual(
    submission,
    expected,
    'member could query the submission'
  )

  // 6. the kaitiaki can query the members submission
  submission = await kaitiaki.getSubmission(submissionId)

  t.deepEqual(
    submission,
    expected,
    'kaitiaki could query the submission'
  )
})
