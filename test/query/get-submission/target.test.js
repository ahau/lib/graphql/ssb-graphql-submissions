const test = require('tape')
const { isMsgId } = require('ssb-ref')
const { promisify: p } = require('util')

const TestBot = require('../../test-bot')
const { sleep, handleErr, GetSubmission, PROPOSE_EDIT_MUTATION, PROPOSE_NEW_MUTATION } = require('../../lib/helpers')

const query = `
query ($id: String!) {
  getSubmission (id: $id) {
    id
    sourceId
    targetType

    ...on SubmissionGroupPerson {
      sourceRecord {
        id
        preferredName
        legalName
        recps
        tombstone {
          date
          reason
        }
      }
      details {
        preferredName
        legalName
        recps
        tombstone {
          date
          reason
        }
      }
    }
  }
}
`

test('proposeEdit person/group target', async (t) => {
  t.plan(7)

  const kaitiaki = await TestBot({ name: 'kaitiaki' })
  const member = await TestBot({ name: 'member' })

  kaitiaki.getSubmission = GetSubmission(kaitiaki, t, query)
  member.getSubmission = GetSubmission(member, t, query)

  t.teardown(() => {
    kaitiaki.close()
    member.close()
  })

  let submission

  // 1. kaitiaki creates a group
  const { groupId } = await p(kaitiaki.ssb.tribes.create)({ addPOBox: true })

  // 2. kaitiaki invites the member to the group
  await p(kaitiaki.ssb.tribes.invite)(groupId, [member.id], {})

  // 3. kaitiaki creates a profile in the group
  const profileDetails = {
    preferredName: 'Cherese',
    legalName: 'Cherese',
    authors: {
      add: ['*']
    },
    recps: [groupId]
  }

  const profileId = await p(kaitiaki.ssb.profile.person.group.create)(profileDetails)
  t.true(isMsgId(profileId), 'kaitiaki creates a profile')

  // replicate to the member
  await kaitiaki.replicate(member)
  await member.replicate(kaitiaki)

  // give it a little time to rebuild
  await sleep(500)

  // 4. member creates a submission to edit the profile
  const res = await member.apollo.mutate({
    mutation: PROPOSE_EDIT_MUTATION,
    variables: {
      profileId,
      input: {
        legalName: 'Cherese Eriepa'
      },
      comment: 'Can you please amend the name as its wrong?',
      groupId
    }
  })
    .catch(handleErr)

  t.error(res.errors, 'proposeEditGroupPerson profile without error')

  const submissionId = res.data.proposeEditGroupPerson
  t.true(isMsgId(submissionId), 'proposeEditGroupPerson returned msgId for the new submission')

  await member.replicate(kaitiaki)

  // 5. the member can query their submission
  submission = await member.getSubmission(submissionId)

  const expected = {
    id: submissionId,
    sourceId: profileId,
    targetType: 'profile/person',
    sourceRecord: {
      id: profileId,
      preferredName: 'Cherese',
      legalName: 'Cherese',
      recps: [groupId],
      tombstone: null
    },
    details: {
      legalName: 'Cherese Eriepa',

      // NOTE: the query returns these fields as null
      // since they werent populated in the submission
      preferredName: null,
      recps: null,
      tombstone: null
    }
  }

  t.deepEqual(
    submission,
    expected,
    'member could query the submission'
  )

  // 6. the kaitiaki can query the members submission
  submission = await kaitiaki.getSubmission(submissionId)

  t.deepEqual(
    submission,
    expected,
    'kaitiaki could query the submission'
  )
})

test('proposeNew person/group target', async (t) => {
  t.plan(6)

  const kaitiaki = await TestBot({ name: 'kaitiaki' })
  const member = await TestBot({ name: 'member' })

  kaitiaki.getSubmission = GetSubmission(kaitiaki, t, query)
  member.getSubmission = GetSubmission(member, t, query)

  t.teardown(() => {
    kaitiaki.close()
    member.close()
  })

  let submission

  // 1. kaitiaki creates a group
  const { groupId } = await p(kaitiaki.ssb.tribes.create)({ addPOBox: true })

  // 2. kaitiaki invites the member to the group
  await p(kaitiaki.ssb.tribes.invite)(groupId, [member.id], {})
  await kaitiaki.replicate(member)
  await member.replicate(kaitiaki)

  // give it a little time to rebuild
  await sleep(500)

  // 3. member creates a submission for a new profile
  const res = await member.apollo.mutate({
    mutation: PROPOSE_NEW_MUTATION,
    variables: {
      input: {
        preferredName: 'bobo',
        recps: [groupId]
      },
      comment: 'Can i please make this new profile for my mum bobo?',
      groupId
    }
  })
    .catch(handleErr)

  t.error(res.errors, 'proposeNewGroupPerson profile without error')

  const submissionId = res.data.proposeNewGroupPerson
  t.true(isMsgId(submissionId), 'proposeNewGroupPerson returned msgId for the new submission')

  await member.replicate(kaitiaki)

  // 4. the member can query their submission
  submission = await member.getSubmission(submissionId)

  const expected = {
    id: submissionId,
    sourceId: null,
    targetType: 'profile/person/admin',
    sourceRecord: null, // because there is no existing profile
    details: {
      preferredName: 'bobo',
      recps: [groupId],

      // default
      legalName: null,
      tombstone: null
    }
  }

  t.deepEqual(
    submission,
    expected,
    'member could query the submission'
  )

  // 5. the kaitiaki can query the members submission
  submission = await kaitiaki.getSubmission(submissionId)

  t.deepEqual(
    submission,
    expected,
    'kaitiaki could query the submission'
  )
})
