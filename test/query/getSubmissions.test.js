const test = require('tape')
const { isMsgId } = require('ssb-ref')
const { promisify: p } = require('util')

const TestBot = require('../test-bot')
const { sleep, handleErr, GetSubmissions, PROPOSE_EDIT_MUTATION } = require('../lib/helpers')

const getSubmissionsQuery = `
  query {
    getSubmissions {
      id
      sourceId
      targetType
      groupId
      ...on SubmissionGroupPerson {
        details {
          preferredName
          legalName
          gender
        }
      }

      ...on SubmissionWhakapapaLink {
        details {
          parent
          child
          relationshipType
          legallyAdopted
        }
      }

      applicantId
      applicant {
        id
        preferredName
      }
    }
  }
`

test('getSubmissions', async (t) => {
  t.plan(15)

  const kaitiaki = await TestBot({ name: 'kaitiaki' })
  const member = await TestBot({ name: 'member' })

  kaitiaki.getSubmissions = GetSubmissions(kaitiaki, t, getSubmissionsQuery)
  member.getSubmissions = GetSubmissions(member, t, getSubmissionsQuery)

  t.teardown(() => {
    kaitiaki.close()
    member.close()
  })

  let res
  let submissions

  // member creates and links a public profile
  const publicProfileId = await p(member.ssb.profile.person.public.create)({ preferredName: 'Cherese', authors: { add: [member.id] } })
  await p(member.ssb.profile.link.create)(publicProfileId)

  // 1. kaitiaki creates a group
  const { groupId, poBoxId } = await p(kaitiaki.ssb.tribes.create)({ addPOBox: true })

  // 2. kaitiaki invites the member to the group
  await p(kaitiaki.ssb.tribes.invite)(groupId, [member.id], {})

  // 3. kaitiaki creates a profile in the group
  const profileDetails = {
    preferredName: 'Cherese',
    legalName: 'Cherese',
    authors: {
      add: ['*']
    },
    recps: [groupId]
  }

  const profileId = await p(kaitiaki.ssb.profile.person.group.create)(profileDetails)
  t.true(isMsgId(profileId), 'kaitiaki creates a profile')

  // replicate to the member
  await kaitiaki.replicate(member)
  await member.replicate(kaitiaki)

  // give it a little time to rebuild
  await sleep(500)

  // 4. member creates multiple submissions

  async function createSubmission (input) {
    res = await member.apollo.mutate({
      mutation: PROPOSE_EDIT_MUTATION,
      variables: {
        profileId,
        input,
        groupId
      }
    })
      .catch(handleErr)

    t.error(res.errors, 'proposeEditGroupPerson profile without error')

    const submissionId = res.data.proposeEditGroupPerson
    t.true(isMsgId(submissionId), 'proposeEditGroupPerson returned msgId for the new submission')

    return {
      id: submissionId,
      sourceId: profileId,
      targetType: 'profile/person',
      applicantId: member.id,
      groupId,
      applicant: {
        id: publicProfileId,
        preferredName: 'Cherese'
      },
      details: {
        preferredName: null,
        legalName: null,
        gender: null,
        ...input
      }
    }
  }

  const expectedSubmissions = await Promise.all(
    [
      createSubmission({ preferredName: 'Reese' }),
      createSubmission({ legalName: 'Cherese Eriepa' }),
      createSubmission({ gender: 'female' })
    ]
  )

  res = await Promise.all(
    expectedSubmissions.map(({ id }) => {
      return member.ssb.submissions.get(id)
    })
  )

  await member.replicate(kaitiaki)

  // 5. the member can query their submissions
  submissions = await member.getSubmissions()

  t.deepEquals(
    submissions,
    expectedSubmissions.reverse(),
    'member can query all submissions'
  )

  // // 6. the kaitiaki can query the members submission
  submissions = await kaitiaki.getSubmissions()

  // // TODO
  t.deepEqual(
    submissions,
    expectedSubmissions,
    'kaitiaki can query all submissions'
  )

  const submissionIdA = submissions[0].id

  // tombstone a linked submission
  await p(member.ssb.submissions.tombstone)(submissionIdA, { reason: 'deleted' })

  let updatedSubmissions = await member.getSubmissions()

  t.deepEqual(
    updatedSubmissions,
    [submissions[1], submissions[2]],
    'tombstoned items are removed'
  )

  /*
    NOTE: this is an issue im running into in the frontend, where I previously created
    invalid submissions (during smoke testing), and now, they are vausing the frontend to
    break complaining that they fail against the spec
  */

  // create an invalid submission
  const invalidContent = {
    type: 'submissions',
    targetId: profileId,
    targetType: 'profile/person',
    details: {
      preferredName: 'TEST'
    },
    groupId,
    comments: {
      [member.id]: 'PLS EDIT'
    },
    recps: [
      poBoxId,
      member.id
    ],
    tangles: {
      submissions: {
        root: null,
        previous: null
      }
    }
  }

  await p(member.ssb.tribes.publish)(invalidContent)

  updatedSubmissions = await member.getSubmissions()

  t.deepEqual(
    updatedSubmissions,
    [submissions[1], submissions[2]],
    'invalid submissions arent shown'
  )
})
