const test = require('tape')
// const { isMsgId } = require('ssb-ref')
const { promisify: p } = require('util')

// const TestBot = require('../../test-bot')
const TestGroup = require('../../test-group')
const { ProposeEditWhakapapaView, ApproveEditWhakapapaViewSubmission, GetSubmission } = require('../../lib/helpers')

const query = `
query ($id: String!) {
  getSubmission (id: $id) {
    ...on SubmissionWhakapapaView {
      id
      targetType
      sourceRecord {
        id
        ignoredProfiles
      }
      targetRecord {
        id
        ignoredProfiles
      }
      details {
        ignoredProfilesSubmission {
          add
          remove
        }
      }
      groupId
    }
  }
}
`

test('proposeEditWhakapapaView', async t => {
  const {
    kaitiaki,
    member,
    groupId
  } = await TestGroup()

  kaitiaki.getSubmission = GetSubmission(kaitiaki, t, query)
  member.getSubmission = GetSubmission(member, t, query)

  const proposeEditWhakapapaView = ProposeEditWhakapapaView(member, t)
  const approveEditWhakapapaView = ApproveEditWhakapapaViewSubmission(kaitiaki, t)

  t.teardown(() => {
    kaitiaki.close()
    member.close()
  })

  let submission

  // 1. The kaitiaki creates a small family

  // (A)--(C)
  //  |
  // (B)

  async function createProfile (name) {
    return p(kaitiaki.ssb.profile.person.group.create)({
      preferredName: name,
      authors: {
        add: ['*']
      },
      recps: [groupId]
    })
  }

  // create their profiles
  const profileIdA = await createProfile('A')
  const profileIdB = await createProfile('B')
  const profileIdC = await createProfile('C')

  // create the links between the profiles
  await p(kaitiaki.ssb.whakapapa.link.create)({
    type: 'link/profile-profile/child',
    parent: profileIdA,
    child: profileIdB
  }, { relationshipType: 'birth' })

  await p(kaitiaki.ssb.whakapapa.link.create)({
    type: 'link/profile-profile/partner',
    parent: profileIdA,
    child: profileIdC
  }, { relationshipType: 'married' })

  // 2. The kaitiaki creates a whakapapa tree, with the small family
  const whakapapaId = await p(kaitiaki.ssb.whakapapa.view.create)({
    name: 'ABC Family',
    focus: profileIdA,
    ignoredProfiles: {
      add: [profileIdB]
    },
    mode: 'descendants',
    authors: {
      add: ['*']
    },
    recps: [groupId],
    recordCount: 3
  })

  await kaitiaki.replicate(member)
  await member.replicate(kaitiaki)

  // 3. The member proposes an update to the whakapapa
  const allowedDetails = {
    ignoredProfiles: {
      add: [profileIdC]
    }
  }
  const submissionId = await proposeEditWhakapapaView({
    whakapapaId,
    input: allowedDetails,
    comment: 'I want to hide this person',
    groupId
  })

  await member.replicate(kaitiaki)
  await kaitiaki.replicate(member)

  // 4. can get the submission (before its approved)
  submission = await member.getSubmission(submissionId)

  let expected = {
    id: submissionId,
    targetType: 'whakapapa/view',
    sourceRecord: {
      id: whakapapaId,
      ignoredProfiles: [profileIdB]
    },
    targetRecord: null,
    details: {
      ignoredProfilesSubmission: {
        add: [profileIdC],
        remove: []
      }
    },
    groupId
  }

  t.deepEqual(
    submission,
    expected,
    'member could query the submission (before)'
  )

  // 5. the kaitiaki can query the members submission
  submission = await kaitiaki.getSubmission(submissionId)

  t.deepEqual(
    submission,
    expected,
    'kaitiaki could query the submission (before)'
  )

  // 6. The kaitiaki accepts the proposal, this also handles executing it
  await approveEditWhakapapaView({
    id: submissionId,
    comment: 'OK',
    allowedFields: allowedDetails
  })

  await kaitiaki.replicate(member)
  await member.replicate(kaitiaki)

  // 7. The kaitiaki can see the updated whakapapaView
  let whakapapaView = await kaitiaki.ssb.whakapapa.view.get(whakapapaId)

  t.equals(whakapapaView.key, whakapapaId, 'returns the id of the whakapapa to the kaitiaki')
  t.deepEquals(whakapapaView.ignoredProfiles.sort(), [profileIdB, profileIdC].sort(), 'returns the expected ignored profiles to the kaitiaki')

  whakapapaView = await member.ssb.whakapapa.view.get(whakapapaId)

  t.equals(whakapapaView.key, whakapapaId, 'returns the id of the whakapapa to the member')
  t.deepEquals(whakapapaView.ignoredProfiles.sort(), [profileIdB, profileIdC].sort(), 'returns the expected ignored profiles to the member')

  // 8. test querying the submission with getSubmission
  submission = await member.getSubmission(submissionId)

  t.notEquals(whakapapaId, submission.targetRecord.id, 'the target record has another id')

  expected = {
    id: submissionId,
    targetType: 'whakapapa/view',
    sourceRecord: {
      id: whakapapaId,
      // NOTE: the source record changes because it has since been updated
      ignoredProfiles: [profileIdB, profileIdC].sort()
    },
    targetRecord: {
      id: submission.targetRecord.id,
      ignoredProfiles: [profileIdC]
    },
    details: {
      ignoredProfilesSubmission: {
        add: [profileIdC],
        remove: []
      }
    },
    groupId
  }

  t.deepEqual(
    submission,
    expected,
    'member could query the submission  (after)'
  )

  // 6. the kaitiaki can query the members submission
  submission = await kaitiaki.getSubmission(submissionId)

  t.deepEqual(
    submission,
    expected,
    'kaitiaki could query the submission (after)'
  )
  t.end()
})
