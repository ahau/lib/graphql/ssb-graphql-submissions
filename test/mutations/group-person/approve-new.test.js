const test = require('tape')
const { isMsgId } = require('ssb-ref')
const { promisify: p } = require('util')

const TestBot = require('../../test-bot')
const { sleep, handleErr, GetSubmission, PROPOSE_NEW_MUTATION } = require('../../lib/helpers')

const query = `
  query ($id: String!) {
    getSubmission (id: $id) {
      id
      sourceId
      targetId
      targetType

      groupId

      approvedByIds
      rejectedByIds
      ...on SubmissionGroupPerson {
        details {
          preferredName
          legalName
          altNames: altNamesSubmission {
            add
            remove
          }
          customFields {
            key
            value
          }
          recps
        }
        sourceRecord {
          preferredName
          legalName
          altNames
          customFields {
            key
            value
          }
        }
        targetRecord {
          preferredName
          legalName
          altNames: altNamesSubmission {
            add
            remove
          }
          customFields {
            key
            value
          }
          recps
        }
      }
      comments {
        authorId
        author {
          id
        }
        comment
      }
      tombstone {
        date
        reason
      }
    }
  }
`

test('approve new', async (t) => {
  t.plan(12)

  const kaitiaki = await TestBot({ name: 'kaitiaki' })
  const member = await TestBot({ name: 'member' })

  kaitiaki.getSubmission = GetSubmission(kaitiaki, t, query)
  member.getSubmission = GetSubmission(member, t, query)

  t.teardown(() => {
    kaitiaki.close()
    member.close()
  })

  let res
  let submission

  // 1. kaitiaki creates a group
  const { groupId } = await p(kaitiaki.ssb.tribes.create)({ addPOBox: true })

  // 2. kaitiaki invites the member to the group
  await p(kaitiaki.ssb.tribes.invite)(groupId, [member.id], {})

  // replicate to the member
  await kaitiaki.replicate(member)
  await member.replicate(kaitiaki)

  // give it a little time to rebuild
  await sleep(500)

  // 3. member creates a submission for a new profile
  const defnTime = Date.now().toString()
  const profileDetails = {
    preferredName: 'David',
    altNames: { add: ['James'] },
    legalName: 'David James',
    customFields: [
      { key: defnTime, value: 'Hamburgers' }
    ],
    // TODO: authors are not supported yet
    // authors: {
    //   add: ['*']
    // },
    recps: [groupId]
  }

  res = await member.apollo.mutate({
    mutation: PROPOSE_NEW_MUTATION,
    variables: {
      input: profileDetails,
      comment: 'Can you please create this profile',
      groupId
    }
  })
    .catch(handleErr)

  t.error(res.errors, 'proposeNewGroupPerson profile without error')

  const submissionId = res.data.proposeNewGroupPerson
  t.true(isMsgId(submissionId), 'proposeNewGroupPerson returned msgId for the new submission')

  await member.replicate(kaitiaki)

  let expected = {
    id: submissionId,
    sourceId: null,
    sourceRecord: null,
    targetId: null,
    targetRecord: null,
    targetType: 'profile/person/admin',
    groupId,
    approvedByIds: [],
    rejectedByIds: [],
    details: {
      preferredName: 'David',
      legalName: 'David James',
      altNames: { add: ['James'], remove: [] },
      customFields: [
        { key: defnTime, value: 'Hamburgers' }
      ],
      recps: [groupId]
    },
    comments: [
      {
        authorId: member.id,
        comment: 'Can you please create this profile',
        author: null
      }
    ],
    tombstone: null
  }

  // 4. the member can query their submission
  submission = await member.getSubmission(submissionId)

  t.deepEqual(
    submission,
    expected,
    'member could query the submission'
  )

  // 5. the kaitiaki can query the members submission
  submission = await kaitiaki.getSubmission(submissionId)

  t.deepEqual(
    submission,
    expected,
    'kaitiaki could query the submission'
  )

  // 6. the kaitiaki can approve the members submission
  // member publishes the new profile
  const input = {
    ...profileDetails,
    authors: {
      add: ['*']
    }
  }

  res = await kaitiaki.apollo.mutate({
    mutation: `
      mutation ($id: String!, $comment: String, $allowedFields: PersonProfileInput!) {
        approveNewGroupPerson(id: $id, comment: $comment, allowedFields: $allowedFields)
      }
      `,
    variables: {
      id: submissionId,
      comment: 'Yeah sure',
      allowedFields: input
    }
  })
    .catch(handleErr)

  await kaitiaki.replicate(member)

  t.true(isMsgId(res.data.approveNewGroupPerson), 'approveNewGroupPerson returns a msgId')

  // 7. the kaitiaki sees the submission was approved
  submission = await kaitiaki.getSubmission(submissionId)

  expected = {
    id: submissionId,
    sourceId: null,
    // NOTE: the source profile will reflect the new value
    sourceRecord: null,
    targetId: submission.targetId,
    targetRecord: {
      preferredName: 'David',
      legalName: 'David James',
      altNames: { add: ['James'], remove: [] },
      customFields: [
        { key: defnTime, value: 'Hamburgers' }
      ],
      recps: [groupId]
    },
    targetType: 'profile/person/admin',
    groupId,
    approvedByIds: [kaitiaki.id],
    rejectedByIds: [],
    details: {
      preferredName: 'David',
      legalName: 'David James',
      altNames: { add: ['James'], remove: [] },
      customFields: [
        { key: defnTime, value: 'Hamburgers' }
      ],
      recps: [groupId]
    },
    comments: [
      {
        authorId: member.id,
        comment: 'Can you please create this profile',
        author: null
      },
      {
        authorId: kaitiaki.id,
        comment: 'Yeah sure',
        author: null
      }
    ],
    tombstone: null
  }

  t.deepEqual(
    submission,
    expected,
    'kaitiaki could query the submission'
  )

  t.notEqual(submission.sourceId, submission.targetId, 'target and source ids are not the same')

  // 8. the member sees the submission was approved
  submission = await member.getSubmission(submissionId)

  t.deepEqual(
    submission,
    expected,
    'kaitiaki could query the submission'
  )
})
