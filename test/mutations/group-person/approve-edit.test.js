const test = require('tape')
const { isMsgId } = require('ssb-ref')
const { promisify: p } = require('util')

const TestBot = require('../../test-bot')
const { sleep, handleErr, GetSubmission, PROPOSE_EDIT_MUTATION } = require('../../lib/helpers')

const query = `
  query ($id: String!) {
    getSubmission (id: $id) {
      id
      sourceId
      targetId
      targetType

      groupId
      
      approvedByIds
      rejectedByIds
      ...on SubmissionGroupPerson {
        source
        details {
          preferredName
          legalName
          altNames: altNamesSubmission {
            add
            remove
          }
          customFields {
            key
            value
          }
        }
        sourceRecord {
          preferredName
          legalName
          altNames
          customFields {
            key
            value
          }
        }
        targetRecord {
          preferredName
          legalName
          altNames: altNamesSubmission {
            add
            remove
          }
          customFields {
            key
            value
          }
        }
      }
      comments {
        authorId
        author {
          id
        }
        comment
      }
      tombstone {
        date
        reason
      }
    }
  }
`

test('approve edit', async (t) => {
  t.plan(13)

  const kaitiaki = await TestBot({ name: 'kaitiaki' })
  const member = await TestBot({ name: 'member' })

  kaitiaki.getSubmission = GetSubmission(kaitiaki, t, query)
  member.getSubmission = GetSubmission(member, t, query)

  t.teardown(() => {
    kaitiaki.close()
    member.close()
  })

  let res
  let submission

  // 1. kaitiaki creates a group
  const { groupId } = await p(kaitiaki.ssb.tribes.create)({ addPOBox: true })

  // 2. kaitiaki invites the member to the group
  await p(kaitiaki.ssb.tribes.invite)(groupId, [member.id], {})

  // 3. kaitiaki creates a profile in the group
  const input = {
    preferredName: 'David',
    altNames: { add: ['James'] },
    legalName: 'David James',
    authors: {
      add: ['*']
    },
    recps: [groupId]
  }

  const profileId = await p(kaitiaki.ssb.profile.person.group.create)(input)
  t.true(isMsgId(profileId), 'kaitiaki creates a profile')

  // replicate to the member
  await kaitiaki.replicate(member)
  await member.replicate(kaitiaki)

  // give it a little time to rebuild
  await sleep(500)

  // 4. member creates a submission to edit the profile
  const defnTime = Date.now().toString()
  const updateDetails = {
    preferredName: 'Dave',
    legalName: null,
    customFields: [
      { key: defnTime, value: 'hamburgers' }
    ],
    altNames: {
      add: ['Davey', 'David'],
      remove: ['James']
    }
  }

  res = await member.apollo.mutate({
    mutation: PROPOSE_EDIT_MUTATION,
    variables: {
      profileId,
      input: updateDetails,
      comment: 'Can you please update this profile',
      groupId
    }
  })
    .catch(handleErr)

  t.error(res.errors, 'proposeEditGroupPerson profile without error')

  const submissionId = res.data.proposeEditGroupPerson
  t.true(isMsgId(submissionId), 'proposeEditGroupPerson returned msgId for the new submission')

  await member.replicate(kaitiaki)

  let expected = {
    id: submissionId,
    sourceId: profileId,
    sourceRecord: {
      preferredName: 'David',
      legalName: 'David James',
      altNames: ['James'],
      customFields: []
    },
    targetId: null,
    targetRecord: null,
    targetType: 'profile/person',
    groupId,
    source: 'ahau',
    approvedByIds: [],
    rejectedByIds: [],
    details: updateDetails,
    comments: [
      {
        authorId: member.id,
        comment: 'Can you please update this profile',
        author: null
      }
    ],
    tombstone: null
  }

  // 5. the member can query their submission
  submission = await member.getSubmission(submissionId)

  t.deepEqual(
    submission,
    expected,
    'member could query the submission'
  )

  // 6. the kaitiaki can query the members submission
  submission = await kaitiaki.getSubmission(submissionId)

  t.deepEqual(
    submission,
    expected,
    'kaitiaki could query the submission'
  )

  // TODO: add step in here where the member cannot approve their own submission

  // 7. the kaitiaki can approve the members submission
  // member publishes the update
  // const updateId = await p(kaitiaki.ssb.profile.person.group.update)(profileId, fixInput.person(updateDetails))

  res = await kaitiaki.apollo.mutate({
    mutation: `
      mutation ($id: String!, $comment: String, $allowedFields: PersonProfileInput!) {
        approveEditGroupPerson(id: $id, comment: $comment, allowedFields: $allowedFields)
      }
      `,
    variables: {
      id: submissionId,
      comment: 'Yeah sure',
      allowedFields: updateDetails
    }
  })
    .catch(handleErr)

  await kaitiaki.replicate(member)

  t.true(isMsgId(res.data.approveEditGroupPerson), 'approveEditGroupPerson returns a msgId')

  // 8. the kaitiaki sees the submission was approved
  submission = await kaitiaki.getSubmission(submissionId)

  expected = {
    id: submissionId,
    sourceId: profileId,
    // NOTE: the source profile will reflect the new value
    sourceRecord: {
      preferredName: 'Dave',
      legalName: null,
      altNames: ['Davey', 'David'],
      customFields: [
        { key: defnTime, value: 'hamburgers' }
      ]
    },
    targetId: submission.targetId,
    targetRecord: updateDetails,
    targetType: 'profile/person',
    groupId,
    source: 'ahau',
    approvedByIds: [kaitiaki.id],
    rejectedByIds: [],
    details: updateDetails,
    comments: [
      {
        authorId: member.id,
        comment: 'Can you please update this profile',
        author: null
      },
      {
        authorId: kaitiaki.id,
        comment: 'Yeah sure',
        author: null
      }
    ],
    tombstone: null
  }

  t.deepEqual(
    submission,
    expected,
    'kaitiaki could query the submission'
  )

  t.notEqual(submission.sourceId, submission.targetId, 'target and source ids are not the same')

  // 9. the member sees the submission was approved
  submission = await member.getSubmission(submissionId)

  t.deepEqual(
    submission,
    expected,
    'kaitiaki could query the submission'
  )
})
