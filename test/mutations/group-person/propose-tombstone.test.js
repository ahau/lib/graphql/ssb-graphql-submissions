const test = require('tape')
const { isMsgId } = require('ssb-ref')
const { promisify: p } = require('util')

const TestBot = require('../../test-bot')
const { sleep, handleErr, GetSubmission, PROPOSE_TOMBSTONE_MUTATION } = require('../../lib/helpers')

test('proposeTombstone, get, approve profile/person group profile submission', async (t) => {
  t.plan(12)

  const kaitiaki = await TestBot({ name: 'kaitiaki' })
  const member = await TestBot({ name: 'member' })

  kaitiaki.getSubmission = GetSubmission(kaitiaki, t)
  member.getSubmission = GetSubmission(member, t)

  t.teardown(() => {
    kaitiaki.close()
    member.close()
  })

  let res
  let submission

  // 1. kaitiaki creates a group
  const { groupId } = await p(kaitiaki.ssb.tribes.create)({ addPOBox: true })

  // 2. kaitiaki invites the member to the group
  await p(kaitiaki.ssb.tribes.invite)(groupId, [member.id], {})

  // 3. kaitiaki creates a profile in the group
  const profileDetails = {
    preferredName: 'Cherese',
    legalName: 'Cherese',
    authors: {
      add: ['*']
    },
    recps: [groupId]
  }

  const profileId = await p(kaitiaki.ssb.profile.person.group.create)(profileDetails)
  t.true(isMsgId(profileId), 'kaitiaki creates a profile')

  // replicate to the member
  await kaitiaki.replicate(member)
  await member.replicate(kaitiaki)

  // give it a little time to rebuild
  await sleep(500)

  // 4. member creates a submission to tombstone the profile
  res = await member.apollo.mutate({
    mutation: PROPOSE_TOMBSTONE_MUTATION,
    variables: {
      recordId: profileId,
      comment: 'Can you please delete this profile?',
      groupId
    }
  })
    .catch(handleErr)

  t.error(res.errors, 'proposeTombstone profile without error')

  const submissionId = res.data.proposeTombstone
  t.true(isMsgId(submissionId), 'proposeTombstone returned msgId for the new submission')

  await member.replicate(kaitiaki)

  // 5. the member can query their submission
  submission = await member.getSubmission(submissionId)

  t.deepEqual(
    submission,
    {
      id: submissionId,
      sourceId: profileId,
      targetId: null,
      targetType: 'profile/person',
      groupId,
      source: 'ahau',
      approvedByIds: [],
      rejectedByIds: [],
      details: {
        tombstone: {
          date: submission.details.tombstone.date,
          reason: null
        },

        // NOTE: the query returns these fields as null
        // since they werent populated in the submission
        preferredName: null,
        legalName: null,
        recps: null
      },
      comments: [
        {
          authorId: member.id,
          comment: 'Can you please delete this profile?',
          author: null
        }
      ],
      tombstone: null
    },
    'member could query the submission'
  )

  // 6. the kaitiaki can query the members submission
  submission = await kaitiaki.getSubmission(submissionId)

  t.deepEqual(
    submission,
    {
      id: submissionId,
      sourceId: profileId,
      targetId: null,
      targetType: 'profile/person',
      groupId,
      source: 'ahau',
      approvedByIds: [],
      rejectedByIds: [],
      details: {
        tombstone: {
          date: submission.details.tombstone.date,
          reason: null
        },

        // NOTE: the query returns these fields as null
        // since they werent populated in the submission
        preferredName: null,
        legalName: null,
        recps: null
      },
      comments: [
        {
          authorId: member.id,
          comment: 'Can you please delete this profile?',
          author: null
        }
      ],
      tombstone: null
    },
    'kaitiaki could query the submission'
  )

  // TODO: add step in here where the member cannot approve their own submission

  // 7. the kaitiaki can approve the members submission
  res = await kaitiaki.apollo.mutate({
    mutation: `
      mutation ($id: String!, $comment: String) {
        approveSubmission(id: $id, comment: $comment)
      }
      `,
    variables: {
      id: submissionId,
      comment: 'Yeah sure'
    }
  })
    .catch(handleErr)

  await kaitiaki.replicate(member)

  t.true(isMsgId(res.data.approveSubmission), 'approveSubmission returns a msgId')

  // TODO: update approve in ssb-submissions to always return the same id?
  // t.equal(submissionId, res.data.approveSubmission, 'approveSubmission returns the submissionId')

  // 8. the kaitiaki sees the submission was approved
  submission = await kaitiaki.getSubmission(submissionId)

  t.deepEqual(
    submission,
    {
      id: submissionId,
      sourceId: profileId,
      targetId: null,
      targetType: 'profile/person',
      groupId,
      source: 'ahau',
      approvedByIds: [kaitiaki.id],
      rejectedByIds: [],
      details: {
        tombstone: {
          date: submission.details.tombstone.date,
          reason: null
        },

        // NOTE: the query returns these fields as null
        // since they werent populated in the submission
        preferredName: null,
        legalName: null,
        recps: null
      },
      comments: [
        {
          authorId: member.id,
          comment: 'Can you please delete this profile?',
          author: null
        },
        {
          authorId: kaitiaki.id,
          comment: 'Yeah sure',
          author: null
        }
      ],
      tombstone: null
    },
    'kaitiaki could query the submission'
  )

  // 9. the member sees the submission was approved
  submission = await member.getSubmission(submissionId)

  t.deepEqual(
    submission,
    {
      id: submissionId,
      sourceId: profileId,
      targetId: null,
      targetType: 'profile/person',
      groupId,
      source: 'ahau',
      approvedByIds: [kaitiaki.id],
      rejectedByIds: [],
      details: {
        tombstone: {
          date: submission.details.tombstone.date,
          reason: null
        },

        // NOTE: the query returns these fields as null
        // since they werent populated in the submission
        preferredName: null,
        legalName: null,
        recps: null
      },
      comments: [
        {
          authorId: member.id,
          comment: 'Can you please delete this profile?',
          author: null
        },
        {
          authorId: kaitiaki.id,
          comment: 'Yeah sure',
          author: null
        }
      ],
      tombstone: null
    },
    'kaitiaki could query the submission'
  )
})

test('proposeTombstone, get, reject profile/person group profile submission', async (t) => {
  t.plan(12)

  const kaitiaki = await TestBot({ name: 'kaitiaki' })
  const member = await TestBot({ name: 'member' })

  kaitiaki.getSubmission = GetSubmission(kaitiaki, t)
  member.getSubmission = GetSubmission(member, t)

  t.teardown(() => {
    kaitiaki.close()
    member.close()
  })

  let res
  let submission

  // 1. kaitiaki creates a group
  const { groupId } = await p(kaitiaki.ssb.tribes.create)({ addPOBox: true })

  // 2. kaitiaki invites the member to the group
  await p(kaitiaki.ssb.tribes.invite)(groupId, [member.id], {})

  // 3. kaitiaki creates a profile in the group
  const profileDetails = {
    preferredName: 'Cherese',
    legalName: 'Cherese',
    authors: {
      add: ['*']
    },
    recps: [groupId]
  }

  const profileId = await p(kaitiaki.ssb.profile.person.group.create)(profileDetails)
  t.true(isMsgId(profileId), 'kaitiaki creates a profile')

  // replicate to the member
  await kaitiaki.replicate(member)
  await member.replicate(kaitiaki)

  // give it a little time to rebuild
  await sleep(500)

  // 4. member creates a submission to tombstone the profile
  res = await member.apollo.mutate({
    mutation: PROPOSE_TOMBSTONE_MUTATION,
    variables: {
      recordId: profileId,
      comment: 'Can you please delete this profile?',
      groupId
    }
  })
    .catch(handleErr)

  t.error(res.errors, 'proposeTombstone profile without error')

  const submissionId = res.data.proposeTombstone
  t.true(isMsgId(submissionId), 'proposeTombstone returned msgId for the new submission')

  await member.replicate(kaitiaki)

  // 5. the member can query their submission
  submission = await member.getSubmission(submissionId)

  t.deepEqual(
    submission,
    {
      id: submissionId,
      sourceId: profileId,
      targetId: null,
      targetType: 'profile/person',
      groupId,
      source: 'ahau',
      approvedByIds: [],
      rejectedByIds: [],
      details: {
        tombstone: {
          date: submission.details.tombstone.date,
          reason: null
        },

        // NOTE: the query returns these fields as null
        // since they werent populated in the submission
        preferredName: null,
        legalName: null,
        recps: null
      },
      comments: [
        {
          authorId: member.id,
          comment: 'Can you please delete this profile?',
          author: null
        }
      ],
      tombstone: null
    },
    'member could query the submission'
  )

  // 6. the kaitiaki can query the members submission
  submission = await kaitiaki.getSubmission(submissionId)

  t.deepEqual(
    submission,
    {
      id: submissionId,
      sourceId: profileId,
      targetId: null,
      targetType: 'profile/person',
      groupId,
      source: 'ahau',
      approvedByIds: [],
      rejectedByIds: [],
      details: {
        tombstone: {
          date: submission.details.tombstone.date,
          reason: null
        },

        // NOTE: the query returns these fields as null
        // since they werent populated in the submission
        preferredName: null,
        legalName: null,
        recps: null
      },
      comments: [
        {
          authorId: member.id,
          comment: 'Can you please delete this profile?',
          author: null
        }
      ],
      tombstone: null
    },
    'kaitiaki could query the submission'
  )

  // TODO: add step in here where the member cannot reject their own submission

  // 7. the kaitiaki can reject the members submission
  res = await kaitiaki.apollo.mutate({
    mutation: `
      mutation ($id: String!, $comment: String) {
        rejectSubmission(id: $id, comment: $comment)
      }
      `,
    variables: {
      id: submissionId,
      comment: 'No sorry, rejected'
    }
  })
    .catch(handleErr)

  await kaitiaki.replicate(member)

  t.true(isMsgId(res.data.rejectSubmission), 'rejectSubmission returns a msgId')

  // TODO: update reject in ssb-submissions to always return the same id?
  // t.equal(submissionId, res.data.rejectSubmission, 'rejectSubmission returns the submissionId')

  // 8. the kaitiaki sees the submission was rejected
  submission = await kaitiaki.getSubmission(submissionId)

  t.deepEqual(
    submission,
    {
      id: submissionId,
      sourceId: profileId,
      targetId: null,
      targetType: 'profile/person',
      groupId,
      source: 'ahau',
      approvedByIds: [],
      rejectedByIds: [kaitiaki.id],
      details: {
        tombstone: {
          date: submission.details.tombstone.date,
          reason: null
        },

        // NOTE: the query returns these fields as null
        // since they werent populated in the submission
        preferredName: null,
        legalName: null,
        recps: null
      },
      comments: [
        {
          authorId: member.id,
          comment: 'Can you please delete this profile?',
          author: null
        },
        {
          authorId: kaitiaki.id,
          comment: 'No sorry, rejected',
          author: null
        }
      ],
      tombstone: null
    },
    'kaitiaki could query the submission'
  )

  // 9. the member sees the submission was rejected
  submission = await member.getSubmission(submissionId)

  t.deepEqual(
    submission,
    {
      id: submissionId,
      sourceId: profileId,
      targetId: null,
      targetType: 'profile/person',
      groupId,
      source: 'ahau',
      approvedByIds: [],
      rejectedByIds: [kaitiaki.id],
      details: {
        tombstone: {
          date: submission.details.tombstone.date,
          reason: null
        },

        // NOTE: the query returns these fields as null
        // since they werent populated in the submission
        preferredName: null,
        legalName: null,
        recps: null
      },
      comments: [
        {
          authorId: member.id,
          comment: 'Can you please delete this profile?',
          author: null
        },
        {
          authorId: kaitiaki.id,
          comment: 'No sorry, rejected',
          author: null
        }
      ],
      tombstone: null
    },
    'member could query the submission'
  )
})

test('proposeTombstone, get, tombstone profile/person group profile submission', async (t) => {
  t.plan(13)

  const kaitiaki = await TestBot({ name: 'kaitiaki' })
  const member = await TestBot({ name: 'member' })

  kaitiaki.getSubmission = GetSubmission(kaitiaki, t)
  member.getSubmission = GetSubmission(member, t)

  t.teardown(() => {
    kaitiaki.close()
    member.close()
  })

  let res
  let submission

  // 1. kaitiaki creates a group
  const { groupId } = await p(kaitiaki.ssb.tribes.create)({ addPOBox: true })

  // 2. kaitiaki invites the member to the group
  await p(kaitiaki.ssb.tribes.invite)(groupId, [member.id], {})

  // 3. kaitiaki creates a profile in the group
  const profileDetails = {
    preferredName: 'Cherese',
    legalName: 'Cherese',
    authors: {
      add: ['*']
    },
    recps: [groupId]
  }

  const profileId = await p(kaitiaki.ssb.profile.person.group.create)(profileDetails)
  t.true(isMsgId(profileId), 'kaitiaki creates a profile')

  // replicate to the member
  await kaitiaki.replicate(member)
  await member.replicate(kaitiaki)

  // give it a little time to rebuild
  await sleep(500)

  // 4. member creates a submission to tombstone the profile
  res = await member.apollo.mutate({
    mutation: PROPOSE_TOMBSTONE_MUTATION,
    variables: {
      recordId: profileId,
      comment: 'Can you please delete this profile?',
      groupId
    }
  })
    .catch(handleErr)

  t.error(res.errors, 'proposeTombstone profile without error')

  const submissionId = res.data.proposeTombstone
  t.true(isMsgId(submissionId), 'proposeTombstone returned msgId for the new submission')

  await member.replicate(kaitiaki)

  // 5. the member can query their submission
  submission = await member.getSubmission(submissionId)

  t.deepEqual(
    submission,
    {
      id: submissionId,
      sourceId: profileId,
      targetId: null,
      targetType: 'profile/person',
      groupId,
      source: 'ahau',
      approvedByIds: [],
      rejectedByIds: [],
      details: {
        tombstone: {
          date: submission.details.tombstone.date,
          reason: null
        },

        // NOTE: the query returns these fields as null
        // since they werent populated in the submission
        preferredName: null,
        legalName: null,
        recps: null
      },
      comments: [
        {
          authorId: member.id,
          comment: 'Can you please delete this profile?',
          author: null
        }
      ],
      tombstone: null
    },
    'member could query the submission'
  )

  // 6. the kaitiaki can query the members submission
  submission = await kaitiaki.getSubmission(submissionId)

  t.deepEqual(
    submission,
    {
      id: submissionId,
      sourceId: profileId,
      targetId: null,
      targetType: 'profile/person',
      groupId,
      source: 'ahau',
      approvedByIds: [],
      rejectedByIds: [],
      details: {
        tombstone: {
          date: submission.details.tombstone.date,
          reason: null
        },

        // NOTE: the query returns these fields as null
        // since they werent populated in the submission
        preferredName: null,
        legalName: null,
        recps: null
      },
      comments: [
        {
          authorId: member.id,
          comment: 'Can you please delete this profile?',
          author: null
        }
      ],
      tombstone: null
    },
    'kaitiaki could query the submission'
  )

  // 7. the kaitiaki can tombstone the members submission
  res = await kaitiaki.apollo.mutate({
    mutation: `
      mutation ($id: String!, $tombstoneInput: TombstoneInput!) {
        tombstoneSubmission(id: $id, tombstoneInput: $tombstoneInput)
      }
      `,
    variables: {
      id: submissionId,
      tombstoneInput: {
        reason: 'Im deleting this'
      }
    }
  })
    .catch(handleErr)

  await kaitiaki.replicate(member)

  t.true(isMsgId(res.data.tombstoneSubmission), 'tombstoneSubmission returns a msgId')
  t.equal(submissionId, res.data.tombstoneSubmission, 'tombstoneSubmission returns the submissionId')

  // 8. the kaitiaki sees the submission was tombstoned
  submission = await kaitiaki.getSubmission(submissionId)

  t.deepEqual(
    submission,
    {
      id: submissionId,
      sourceId: profileId,
      targetId: null,
      targetType: 'profile/person',
      groupId,
      source: 'ahau',
      approvedByIds: [],
      rejectedByIds: [],
      details: {
        tombstone: {
          date: submission.details.tombstone.date,
          reason: null
        },

        // NOTE: the query returns these fields as null
        // since they werent populated in the submission
        preferredName: null,
        legalName: null,
        recps: null
      },
      comments: [
        {
          authorId: member.id,
          comment: 'Can you please delete this profile?',
          author: null
        }
      ],
      tombstone: {
        date: submission.tombstone.date,
        reason: 'Im deleting this'
      }
    },
    'kaitiaki could query the submission'
  )

  // 9. the member sees the submission was tombstoned
  submission = await member.getSubmission(submissionId)

  t.deepEqual(
    submission,
    {
      id: submissionId,
      sourceId: profileId,
      targetId: null,
      targetType: 'profile/person',
      groupId,
      source: 'ahau',
      approvedByIds: [],
      rejectedByIds: [],
      details: {
        tombstone: {
          date: submission.details.tombstone.date,
          reason: null
        },

        // NOTE: the query returns these fields as null
        // since they werent populated in the submission
        preferredName: null,
        legalName: null,
        recps: null
      },
      comments: [
        {
          authorId: member.id,
          comment: 'Can you please delete this profile?',
          author: null
        }
      ],
      tombstone: {
        date: submission.tombstone.date,
        reason: 'Im deleting this'
      }
    },
    'member could query the submission'
  )
})
