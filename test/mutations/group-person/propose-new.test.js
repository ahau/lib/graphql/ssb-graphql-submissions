const test = require('tape')
const { isMsgId } = require('ssb-ref')
const { promisify: p } = require('util')

const TestBot = require('../../test-bot')
const { sleep, handleErr, GetSubmission, PROPOSE_NEW_MUTATION } = require('../../lib/helpers')

test('proposeNew, get, approve profile/person group profile submission', async (t) => {
  t.plan(11)

  const kaitiaki = await TestBot({ name: 'kaitiaki' })
  const member = await TestBot({ name: 'member' })

  // kaitiaki.ssb.submissions.registerHandler(kaitiaki.ssb.profile.person.group)
  // member.ssb.submissions.registerHandler(member.ssb.profile.person.group)

  kaitiaki.getSubmission = GetSubmission(kaitiaki, t)
  member.getSubmission = GetSubmission(member, t)

  t.teardown(() => {
    kaitiaki.close()
    member.close()
  })

  let res
  let submission
  let expected

  // create and link a public profile for the kaitiaki
  const kaitiakiPublicProfileId = await p(kaitiaki.ssb.profile.person.public.create)({ preferredName: 'Mix', authors: { add: [kaitiaki.id] } })
  await p(kaitiaki.ssb.profile.link.create)(kaitiakiPublicProfileId)

  // create and link a public profile for the member
  const memberPublicProfileId = await p(member.ssb.profile.person.public.create)({ preferredName: 'Cherese', authors: { add: [member.id] } })
  await p(member.ssb.profile.link.create)(memberPublicProfileId)

  // 1. kaitiaki creates a group
  const { groupId } = await p(kaitiaki.ssb.tribes.create)({ addPOBox: true })

  // 2. kaitiaki invites the member to the group
  await p(kaitiaki.ssb.tribes.invite)(groupId, [member.id], {})
  await kaitiaki.replicate(member)
  await member.replicate(kaitiaki)

  // give it a little time to rebuild
  await sleep(500)

  // 3. member creates a submission for a new profile
  res = await member.apollo.mutate({
    mutation: PROPOSE_NEW_MUTATION,
    variables: {
      input: {
        preferredName: 'bobo'
      },
      comment: 'Can i please make this new profile for my mum bobo?',
      groupId,
      source: 'ahau'
    }
  })
    .catch(handleErr)

  t.error(res.errors, 'proposeNewGroupPerson profile without error')

  const submissionId = res.data.proposeNewGroupPerson
  t.true(isMsgId(submissionId), 'proposeNewGroupPerson returned msgId for the new submission')

  await member.replicate(kaitiaki)

  // 4. the member can query their submission
  submission = await member.getSubmission(submissionId)

  expected = {
    id: submissionId,
    sourceId: null,
    targetId: null,
    targetType: 'profile/person/admin',
    groupId,
    source: 'ahau',
    approvedByIds: [],
    rejectedByIds: [],
    details: {
      preferredName: 'bobo',
      recps: [groupId],

      // default
      legalName: null,
      tombstone: null
    },
    comments: [
      {
        authorId: member.id,
        comment: 'Can i please make this new profile for my mum bobo?',
        author: {
          id: memberPublicProfileId
        }
      }
    ],
    tombstone: null
  }

  t.deepEqual(
    submission,
    expected,
    'member could query the submission'
  )

  // 5. the kaitiaki can query the members submission
  submission = await kaitiaki.getSubmission(submissionId)

  t.deepEqual(
    submission,
    expected,
    'kaitiaki could query the submission'
  )

  // TODO: add step in here where the member cannot approve their own submission

  // 6. the kaitiaki can approve the members submission
  res = await kaitiaki.apollo.mutate({
    mutation: `
      mutation ($id: String!, $comment: String) {
        approveSubmission(id: $id, comment: $comment)
      }
      `,
    variables: {
      id: submissionId,
      comment: 'Yeah sure, all approved'
    }
  })
    .catch(handleErr)

  await kaitiaki.replicate(member)

  t.true(isMsgId(res.data.approveSubmission), 'approveSubmission returns a msgId')

  // TODO: update approve in ssb-submissions to always return the same id?
  // t.equal(submissionId, res.data.approveSubmission, 'approveSubmission returns the submissionId')

  // 7. the kaitiaki sees the submission was approved
  submission = await kaitiaki.getSubmission(submissionId)

  expected = {
    id: submissionId,
    sourceId: null,
    targetId: null,
    targetType: 'profile/person/admin',
    groupId,
    source: 'ahau',
    approvedByIds: [kaitiaki.id],
    rejectedByIds: [],
    details: {
      preferredName: 'bobo',
      recps: [groupId],

      // default
      legalName: null,
      tombstone: null
    },
    comments: [
      {
        authorId: member.id,
        comment: 'Can i please make this new profile for my mum bobo?',
        author: {
          id: memberPublicProfileId
        }
      },
      {
        authorId: kaitiaki.id,
        comment: 'Yeah sure, all approved',
        author: {
          id: kaitiakiPublicProfileId
        }
      }
    ],
    tombstone: null
  }

  t.deepEqual(
    submission,
    expected,
    'kaitiaki could query the submission'
  )

  // 8. the member sees the submission was approved
  submission = await member.getSubmission(submissionId)

  t.deepEqual(
    submission,
    expected,
    'kaitiaki could query the submission'
  )
})

test('proposeNew, get, reject profile/person group profile submission', async (t) => {
  t.plan(11)

  const kaitiaki = await TestBot({ name: 'kaitiaki' })
  const member = await TestBot({ name: 'member' })

  kaitiaki.getSubmission = GetSubmission(kaitiaki, t)
  member.getSubmission = GetSubmission(member, t)

  t.teardown(() => {
    kaitiaki.close()
    member.close()
  })

  let res
  let submission

  // 1. kaitiaki creates a group
  const { groupId } = await p(kaitiaki.ssb.tribes.create)({ addPOBox: true })

  // 2. kaitiaki invites the member to the group
  await p(kaitiaki.ssb.tribes.invite)(groupId, [member.id], {})
  await kaitiaki.replicate(member)
  await member.replicate(kaitiaki)

  // give it a little time to rebuild
  await sleep(500)

  // 3. member creates a submission for a new profile
  res = await member.apollo.mutate({
    mutation: PROPOSE_NEW_MUTATION,
    variables: {
      input: {
        preferredName: 'bobo',
        recps: [groupId]
      },
      comment: 'Can i please make this new profile for my mum bobo?',
      groupId
    }
  })
    .catch(handleErr)

  t.error(res.errors, 'proposeNewGroupPerson profile without error')

  const submissionId = res.data.proposeNewGroupPerson
  t.true(isMsgId(submissionId), 'proposeNewGroupPerson returned msgId for the new submission')

  await member.replicate(kaitiaki)

  // 4. the member can query their submission
  submission = await member.getSubmission(submissionId)

  t.deepEqual(
    submission,
    {
      id: submissionId,
      sourceId: null,
      targetId: null,
      targetType: 'profile/person/admin',
      groupId,
      source: 'ahau',
      approvedByIds: [],
      rejectedByIds: [],
      details: {
        preferredName: 'bobo',
        recps: [groupId],

        // default
        legalName: null,
        tombstone: null
      },
      comments: [
        {
          authorId: member.id,
          comment: 'Can i please make this new profile for my mum bobo?',
          author: null
        }
      ],
      tombstone: null
    },
    'member could query the submission'
  )

  // 5. the kaitiaki can query the members submission
  submission = await kaitiaki.getSubmission(submissionId)

  t.deepEqual(
    submission,
    {
      id: submissionId,
      sourceId: null,
      targetId: null,
      targetType: 'profile/person/admin',
      groupId,
      source: 'ahau',
      approvedByIds: [],
      rejectedByIds: [],
      details: {
        preferredName: 'bobo',
        recps: [groupId],

        // default
        legalName: null,
        tombstone: null
      },
      comments: [
        {
          authorId: member.id,
          comment: 'Can i please make this new profile for my mum bobo?',
          author: null
        }
      ],
      tombstone: null
    },
    'kaitiaki could query the submission'
  )

  // TODO: add step in here where the member cannot reject their own submission

  // 6. the kaitiaki can reject the members submission
  res = await kaitiaki.apollo.mutate({
    mutation: `
      mutation ($id: String!, $comment: String) {
        rejectSubmission(id: $id, comment: $comment)
      }
      `,
    variables: {
      id: submissionId,
      comment: 'No sorry, rejected'
    }
  })
    .catch(handleErr)

  await kaitiaki.replicate(member)

  t.true(isMsgId(res.data.rejectSubmission), 'rejectSubmission returns a msgId')

  // TODO: update reject in ssb-submissions to always return the same id?
  // t.equal(submissionId, res.data.rejectSubmission, 'rejectSubmission returns the submissionId')

  // 7. the kaitiaki sees the submission was rejected
  submission = await kaitiaki.getSubmission(submissionId)

  t.deepEqual(
    submission,
    {
      id: submissionId,
      sourceId: null,
      targetId: null,
      targetType: 'profile/person/admin',
      groupId,
      source: 'ahau',
      approvedByIds: [],
      rejectedByIds: [kaitiaki.id],
      details: {
        preferredName: 'bobo',
        recps: [groupId],

        // default
        legalName: null,
        tombstone: null
      },
      comments: [
        {
          authorId: member.id,
          comment: 'Can i please make this new profile for my mum bobo?',
          author: null
        },
        {
          authorId: kaitiaki.id,
          comment: 'No sorry, rejected',
          author: null
        }
      ],
      tombstone: null
    },
    'kaitiaki could query the submission'
  )

  // 8. the member sees the submission was rejected
  submission = await member.getSubmission(submissionId)

  t.deepEqual(
    submission,
    {
      id: submissionId,
      sourceId: null,
      targetId: null,
      targetType: 'profile/person/admin',
      groupId,
      source: 'ahau',
      approvedByIds: [],
      rejectedByIds: [kaitiaki.id],
      details: {
        preferredName: 'bobo',
        recps: [groupId],

        // default
        legalName: null,
        tombstone: null
      },
      comments: [
        {
          authorId: member.id,
          comment: 'Can i please make this new profile for my mum bobo?',
          author: null
        },
        {
          authorId: kaitiaki.id,
          comment: 'No sorry, rejected',
          author: null
        }
      ],
      tombstone: null
    },
    'kaitiaki could query the submission'
  )
})

test('proposeNew, get, tombstone profile/person group profile submission', async (t) => {
  t.plan(12)

  const kaitiaki = await TestBot({ name: 'kaitiaki' })
  const member = await TestBot({ name: 'member' })

  kaitiaki.getSubmission = GetSubmission(kaitiaki, t)
  member.getSubmission = GetSubmission(member, t)

  t.teardown(() => {
    kaitiaki.close()
    member.close()
  })

  let res
  let submission

  // 1. kaitiaki creates a group
  const { groupId } = await p(kaitiaki.ssb.tribes.create)({ addPOBox: true })

  // 2. kaitiaki invites the member to the group
  await p(kaitiaki.ssb.tribes.invite)(groupId, [member.id], {})
  await kaitiaki.replicate(member)
  await member.replicate(kaitiaki)

  // give it a little time to rebuild
  await sleep(500)

  // 3. member creates a submission for a new profile
  res = await member.apollo.mutate({
    mutation: PROPOSE_NEW_MUTATION,
    variables: {
      input: {
        preferredName: 'bobo',
        recps: [groupId]
      },
      comment: 'Can i please make this new profile for my mum bobo?',
      groupId
    }
  })
    .catch(handleErr)

  t.error(res.errors, 'proposeNewGroupPerson profile without error')

  const submissionId = res.data.proposeNewGroupPerson
  t.true(isMsgId(submissionId), 'proposeNewGroupPerson returned msgId for the new submission')

  await member.replicate(kaitiaki)

  // 4. the member can query their submission
  submission = await member.getSubmission(submissionId)

  t.deepEqual(
    submission,
    {
      id: submissionId,
      sourceId: null,
      targetId: null,
      targetType: 'profile/person/admin',
      groupId,
      source: 'ahau',
      approvedByIds: [],
      rejectedByIds: [],
      details: {
        preferredName: 'bobo',
        recps: [groupId],

        // default
        legalName: null,
        tombstone: null
      },
      comments: [
        {
          authorId: member.id,
          comment: 'Can i please make this new profile for my mum bobo?',
          author: null
        }
      ],
      tombstone: null
    },
    'member could query the submission'
  )

  // 5. the kaitiaki can query the members submission
  submission = await kaitiaki.getSubmission(submissionId)

  t.deepEqual(
    submission,
    {
      id: submissionId,
      sourceId: null,
      targetId: null,
      targetType: 'profile/person/admin',
      groupId,
      source: 'ahau',
      approvedByIds: [],
      rejectedByIds: [],
      details: {
        preferredName: 'bobo',
        recps: [groupId],

        // default
        legalName: null,
        tombstone: null
      },
      comments: [
        {
          authorId: member.id,
          comment: 'Can i please make this new profile for my mum bobo?',
          author: null
        }
      ],
      tombstone: null
    },
    'kaitiaki could query the submission'
  )

  // 6. the kaitiaki can tombstone the members submission
  res = await kaitiaki.apollo.mutate({
    mutation: `
      mutation ($id: String!, $tombstoneInput: TombstoneInput!) {
        tombstoneSubmission(id: $id, tombstoneInput: $tombstoneInput)
      }
      `,
    variables: {
      id: submissionId,
      tombstoneInput: {
        reason: 'Im deleting this'
      }
    }
  })
    .catch(handleErr)

  await kaitiaki.replicate(member)

  t.true(isMsgId(res.data.tombstoneSubmission), 'tombstoneSubmission returns a msgId')
  t.equal(submissionId, res.data.tombstoneSubmission, 'tombstoneSubmission returns the submissionId')

  // 7. the kaitiaki sees the submission was tombstoned
  submission = await kaitiaki.getSubmission(submissionId)

  t.deepEqual(
    submission,
    {
      id: submissionId,
      sourceId: null,
      targetId: null,
      targetType: 'profile/person/admin',
      groupId,
      source: 'ahau',
      approvedByIds: [],
      rejectedByIds: [],
      details: {
        preferredName: 'bobo',
        recps: [groupId],

        // default
        legalName: null,
        tombstone: null
      },
      comments: [
        {
          authorId: member.id,
          comment: 'Can i please make this new profile for my mum bobo?',
          author: null
        }
      ],
      tombstone: {
        date: submission.tombstone.date,
        reason: 'Im deleting this'
      }
    },
    'kaitiaki could query the submission'
  )

  // 8. the member sees the submission was tombstoned
  submission = await member.getSubmission(submissionId)

  t.deepEqual(
    submission,
    {
      id: submissionId,
      sourceId: null,
      targetId: null,
      targetType: 'profile/person/admin',
      groupId,
      source: 'ahau',
      approvedByIds: [],
      rejectedByIds: [],
      details: {
        preferredName: 'bobo',
        recps: [groupId],

        // default
        legalName: null,
        tombstone: null
      },
      comments: [
        {
          authorId: member.id,
          comment: 'Can i please make this new profile for my mum bobo?',
          author: null
        }
      ],
      tombstone: {
        date: submission.tombstone.date,
        reason: 'Im deleting this'
      }
    },
    'kaitiaki could query the submission'
  )
})

test('proposeNew (moreInfo.joiningQuestions)', async t => {
  t.plan(6)

  const query = `
    query ($id: String!) {
      getSubmission (id: $id) {
        id
        ...on SubmissionGroupPerson {
          details {
            preferredName
          }
          joiningQuestions {
            label
            value
          }
        }
      }
    }
  `
  const kaitiaki = await TestBot({ name: 'kaitiaki' })
  const member = await TestBot({ name: 'member' })

  kaitiaki.getSubmission = GetSubmission(kaitiaki, t, query)
  member.getSubmission = GetSubmission(member, t, query)

  t.teardown(() => {
    kaitiaki.close()
    member.close()
  })

  let submission

  // 1. kaitiaki creates a group
  const { groupId } = await p(kaitiaki.ssb.tribes.create)({ addPOBox: true })

  // 2. kaitiaki invites the member to the group
  await p(kaitiaki.ssb.tribes.invite)(groupId, [member.id], {})
  await kaitiaki.replicate(member)
  await member.replicate(kaitiaki)

  // give it a little time to rebuild
  await sleep(500)

  // 3. member creates a submission for a new profile
  const res = await member.apollo.mutate({
    mutation: PROPOSE_NEW_MUTATION,
    variables: {
      input: {
        preferredName: 'Cherese',
        recps: [groupId]
      },
      groupId,
      joiningQuestions: [
        {
          label: 'Is this a profile for you?',
          value: 'yes'
        }
      ]
    }
  })
    .catch(handleErr)

  t.error(res.errors, 'proposeNewGroupPerson profile without error')

  const submissionId = res.data.proposeNewGroupPerson
  t.true(isMsgId(submissionId), 'proposeNewGroupPerson returned msgId for the new submission')

  await member.replicate(kaitiaki)

  // 4. the member can query their submission
  submission = await member.getSubmission(submissionId)

  const expected = {
    id: submissionId,
    details: {
      preferredName: 'Cherese'
    },
    joiningQuestions: [
      {
        label: 'Is this a profile for you?',
        value: 'yes'
      }
    ]
  }

  t.deepEqual(
    submission,
    expected,
    'member could query the submission'
  )

  // 5. the kaitiaki can query the members submission
  submission = await kaitiaki.getSubmission(submissionId)

  t.deepEqual(
    submission,
    expected,
    'kaitiaki could query the submission'
  )
})
