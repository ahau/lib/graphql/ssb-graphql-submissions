const test = require('tape')
const { isMsgId } = require('ssb-ref')
const { promisify: p } = require('util')

const TestGroup = require('../../test-group')

const { handleErr, GetSubmission, PROPOSE_NEW_WHAKAPAPA_LINK } = require('../../lib/helpers')

const query = `
  query ($id: String!) {
    getSubmission (id: $id) {
      id
      sourceId
      targetId
      targetType

      groupId

      approvedByIds
      rejectedByIds
      ...on SubmissionWhakapapaLink {
        details {
          parent
          child
          relationshipType
          legallyAdopted
        }
      }
      tombstone {
        date
        reason
      }
    }
  }
`

test('proposeNew, get, approve link/profile-profile/child submission', async (t) => {
  t.plan(13)

  const {
    kaitiaki,
    member,
    groupId
  } = await TestGroup()

  kaitiaki.getSubmission = GetSubmission(kaitiaki, t, query)
  member.getSubmission = GetSubmission(member, t, query)

  t.teardown(() => {
    kaitiaki.close()
    member.close()
  })

  let res
  let submission
  let expected

  // 2. create test profiles to link to
  const mumsProfileId = await p(member.ssb.profile.person.group.create)({
    preferredName: 'Claudine',
    authors: {
      add: ['*']
    },
    recps: [groupId]
  })
  t.true(isMsgId(mumsProfileId), 'creates a profile for the mother')

  const daughtersProfileId = await p(member.ssb.profile.person.group.create)({
    preferredName: 'Cherese',
    authors: {
      add: ['*']
    },
    recps: [groupId]
  })
  t.true(isMsgId(daughtersProfileId), 'creates a profile for the mother')

  // 3. member creates a submission for a new link between the profiles
  res = await member.apollo.mutate({
    mutation: PROPOSE_NEW_WHAKAPAPA_LINK,
    variables: {
      input: {
        type: 'link/profile-profile/child',
        parent: mumsProfileId,
        child: daughtersProfileId,
        relationshipType: 'birth'
      },
      groupId
    }
  })
    .catch(handleErr)

  t.error(res.errors, 'proposeNewWhakapapaLink without error')

  const submissionId = res.data.proposeNewWhakapapaLink
  t.true(isMsgId(submissionId), 'proposeNewWhakapapaLink returned msgId for the new submission')

  await member.replicate(kaitiaki)

  // 4. the member can query their submission
  submission = await member.getSubmission(submissionId)

  expected = {
    id: submissionId,
    sourceId: null,
    targetId: null,
    targetType: 'link/profile-profile/child',
    groupId,
    approvedByIds: [],
    rejectedByIds: [],
    details: {
      parent: mumsProfileId,
      child: daughtersProfileId,
      relationshipType: 'birth',
      legallyAdopted: null
    },
    tombstone: null
  }

  t.deepEqual(
    submission,
    expected,
    'member could query the submission'
  )

  // 5. the kaitiaki can query the members submission
  submission = await kaitiaki.getSubmission(submissionId)

  t.deepEqual(
    submission,
    expected,
    'kaitiaki could query the submission'
  )

  // 6. the kaitiaki can approve the members submission
  res = await kaitiaki.apollo.mutate({
    mutation: `
      mutation ($id: String!, $comment: String) {
        approveSubmission(id: $id, comment: $comment)
      }
      `,
    variables: {
      id: submissionId,
      comment: 'Yeah sure, all approved'
    }
  })
    .catch(handleErr)

  await kaitiaki.replicate(member)

  t.true(isMsgId(res.data.approveSubmission), 'approveSubmission returns a msgId')

  // 7. the kaitiaki sees the submission was approved
  submission = await kaitiaki.getSubmission(submissionId)

  expected = {
    id: submissionId,
    sourceId: null,
    targetId: null,
    targetType: 'link/profile-profile/child',
    groupId,
    approvedByIds: [kaitiaki.id],
    rejectedByIds: [],
    details: {
      parent: mumsProfileId,
      child: daughtersProfileId,
      relationshipType: 'birth',
      legallyAdopted: null
    },
    tombstone: null
  }

  t.deepEqual(
    submission,
    expected,
    'kaitiaki could query the submission'
  )

  // 8. the member sees the submission was approved
  submission = await member.getSubmission(submissionId)

  t.deepEqual(
    submission,
    expected,
    'kaitiaki could query the submission'
  )
})

test('proposeNew, get, reject link/profile-profile/child submission', async (t) => {
  t.plan(13)

  const {
    kaitiaki,
    member,
    groupId
  } = await TestGroup()

  kaitiaki.getSubmission = GetSubmission(kaitiaki, t, query)
  member.getSubmission = GetSubmission(member, t, query)

  t.teardown(() => {
    kaitiaki.close()
    member.close()
  })

  let res
  let submission
  let expected

  // 2. create test profiles to link to
  const mumsProfileId = await p(member.ssb.profile.person.group.create)({
    preferredName: 'Claudine',
    authors: {
      add: ['*']
    },
    recps: [groupId]
  })
  t.true(isMsgId(mumsProfileId), 'creates a profile for the mother')

  const daughtersProfileId = await p(member.ssb.profile.person.group.create)({
    preferredName: 'Cherese',
    authors: {
      add: ['*']
    },
    recps: [groupId]
  })
  t.true(isMsgId(daughtersProfileId), 'creates a profile for the mother')

  // 3. member creates a submission for a new link between the profiles
  res = await member.apollo.mutate({
    mutation: PROPOSE_NEW_WHAKAPAPA_LINK,
    variables: {
      input: {
        type: 'link/profile-profile/child',
        parent: mumsProfileId,
        child: daughtersProfileId,
        relationshipType: 'birth'
      },
      groupId
    }
  })
    .catch(handleErr)

  t.error(res.errors, 'proposeNewWhakapapaLink without error')

  const submissionId = res.data.proposeNewWhakapapaLink
  t.true(isMsgId(submissionId), 'proposeNewWhakapapaLink returned msgId for the new submission')

  await member.replicate(kaitiaki)

  // 4. the member can query their submission
  submission = await member.getSubmission(submissionId)

  expected = {
    id: submissionId,
    sourceId: null,
    targetId: null,
    targetType: 'link/profile-profile/child',
    groupId,
    approvedByIds: [],
    rejectedByIds: [],
    details: {
      parent: mumsProfileId,
      child: daughtersProfileId,
      relationshipType: 'birth',
      legallyAdopted: null
    },
    tombstone: null
  }

  t.deepEqual(
    submission,
    expected,
    'member could query the submission'
  )

  // 5. the kaitiaki can query the members submission
  submission = await kaitiaki.getSubmission(submissionId)

  t.deepEqual(
    submission,
    expected,
    'kaitiaki could query the submission'
  )

  // 6. the kaitiaki can reject the members submission
  res = await kaitiaki.apollo.mutate({
    mutation: `
      mutation ($id: String!, $comment: String) {
        rejectSubmission(id: $id, comment: $comment)
      }
      `,
    variables: {
      id: submissionId
    }
  })
    .catch(handleErr)

  await kaitiaki.replicate(member)

  t.true(isMsgId(res.data.rejectSubmission), 'rejectSubmission returns a msgId')

  // 7. the kaitiaki sees the submission was approved
  submission = await kaitiaki.getSubmission(submissionId)

  expected = {
    id: submissionId,
    sourceId: null,
    targetId: null,
    targetType: 'link/profile-profile/child',
    groupId,
    approvedByIds: [],
    rejectedByIds: [kaitiaki.id],
    details: {
      parent: mumsProfileId,
      child: daughtersProfileId,
      relationshipType: 'birth',
      legallyAdopted: null
    },
    tombstone: null
  }

  t.deepEqual(
    submission,
    expected,
    'kaitiaki could query the submission'
  )

  // 8. the member sees the submission was approved
  submission = await member.getSubmission(submissionId)

  t.deepEqual(
    submission,
    expected,
    'kaitiaki could query the submission'
  )
})

test('proposeNew, get, tombstone link/profile-profile/child submission', async (t) => {
  t.plan(13)

  const {
    kaitiaki,
    member,
    groupId
  } = await TestGroup()

  kaitiaki.getSubmission = GetSubmission(kaitiaki, t, query)
  member.getSubmission = GetSubmission(member, t, query)

  t.teardown(() => {
    kaitiaki.close()
    member.close()
  })

  let res
  let submission
  let expected

  // 2. create test profiles to link to
  const mumsProfileId = await p(member.ssb.profile.person.group.create)({
    preferredName: 'Claudine',
    authors: {
      add: ['*']
    },
    recps: [groupId]
  })
  t.true(isMsgId(mumsProfileId), 'creates a profile for the mother')

  const daughtersProfileId = await p(member.ssb.profile.person.group.create)({
    preferredName: 'Cherese',
    authors: {
      add: ['*']
    },
    recps: [groupId]
  })
  t.true(isMsgId(daughtersProfileId), 'creates a profile for the mother')

  // 3. member creates a submission for a new link between the profiles
  res = await member.apollo.mutate({
    mutation: PROPOSE_NEW_WHAKAPAPA_LINK,
    variables: {
      input: {
        type: 'link/profile-profile/child',
        parent: mumsProfileId,
        child: daughtersProfileId,
        relationshipType: 'birth'
      },
      groupId
    }
  })
    .catch(handleErr)

  t.error(res.errors, 'proposeNewWhakapapaLink without error')

  const submissionId = res.data.proposeNewWhakapapaLink
  t.true(isMsgId(submissionId), 'proposeNewWhakapapaLink returned msgId for the new submission')

  await member.replicate(kaitiaki)

  // 4. the member can query their submission
  submission = await member.getSubmission(submissionId)

  expected = {
    id: submissionId,
    sourceId: null,
    targetId: null,
    targetType: 'link/profile-profile/child',
    groupId,
    approvedByIds: [],
    rejectedByIds: [],
    details: {
      parent: mumsProfileId,
      child: daughtersProfileId,
      relationshipType: 'birth',
      legallyAdopted: null
    },
    tombstone: null
  }

  t.deepEqual(
    submission,
    expected,
    'member could query the submission'
  )

  // 5. the kaitiaki can query the members submission
  submission = await kaitiaki.getSubmission(submissionId)

  t.deepEqual(
    submission,
    expected,
    'kaitiaki could query the submission'
  )

  // 6. the kaitiaki can tombstone the members submission
  res = await kaitiaki.apollo.mutate({
    mutation: `
      mutation ($id: String!, $tombstoneInput: TombstoneInput!) {
        tombstoneSubmission(id: $id, tombstoneInput: $tombstoneInput)
      }
      `,
    variables: {
      id: submissionId,
      tombstoneInput: {
        reason: 'Im deleting this'
      }
    }
  })
    .catch(handleErr)

  await kaitiaki.replicate(member)

  t.true(isMsgId(res.data.tombstoneSubmission), 'tombstoneSubmission returns a msgId')

  // 7. the kaitiaki sees the submission was approved
  submission = await kaitiaki.getSubmission(submissionId)

  expected = {
    id: submissionId,
    sourceId: null,
    targetId: null,
    targetType: 'link/profile-profile/child',
    groupId,
    approvedByIds: [],
    rejectedByIds: [],
    details: {
      parent: mumsProfileId,
      child: daughtersProfileId,
      relationshipType: 'birth',
      legallyAdopted: null
    },
    tombstone: {
      date: submission.tombstone.date,
      reason: 'Im deleting this'
    }
  }

  t.deepEqual(
    submission,
    expected,
    'kaitiaki could query the submission'
  )

  // 8. the member sees the submission was approved
  submission = await member.getSubmission(submissionId)

  t.deepEqual(
    submission,
    expected,
    'kaitiaki could query the submission'
  )
})
