const test = require('tape')
const { promisify: p } = require('util')

const {
  ProposeNewGroupPerson,
  ProposeNewWhakapapaLink,
  CreateSubmissionsLink,
  GetSubmissions,
  SavePerson,
  ApproveSubmission,
  ApproveNewWhakapapaLinkSubmission
} = require('../../lib/helpers')

const TestGroup = require('../../test-group')

const query = `
  query {
    getSubmissions {
      id
      ...on SubmissionGroupPerson {
        details {
          preferredName
        }
      }
      dependencies {
        id
        ...on SubmissionWhakapapaLink {
          details {
            parent
            relationshipType
          }
          sourceRecord {
            type
            parent
            child
            relationshipType
            legallyAdopted
            recps
        
          }
          targetRecord {
            type
            parent
            child
            relationshipType
            legallyAdopted
            recps
          }
        }
      }
    }
  }
`

test('render whakapapa link update', async t => {
  t.plan(14)
  const {
    kaitiaki,
    member,
    groupId
    // poBoxId
  } = await TestGroup({ makeProfiles: true })

  t.teardown(() => {
    kaitiaki.close()
    member.close()
  })

  // set up mutations and queries
  const proposeNewGroupPerson = ProposeNewGroupPerson(member, t)
  const proposeNewWhakapapaLink = ProposeNewWhakapapaLink(member, t)
  const createSubmissionsLink = CreateSubmissionsLink(member, t)
  const getSubmissionsMember = GetSubmissions(member, t, query)

  const savePerson = SavePerson(kaitiaki, t)
  const approveSubmission = ApproveSubmission(kaitiaki, t)
  const approveNewWhakapapaLinkSubmission = ApproveNewWhakapapaLinkSubmission(kaitiaki, t)
  const getSubmissionsKaitiaiki = GetSubmissions(kaitiaki, t, query)

  let submissions

  // 1. Kaitiaki creates a profile in a group
  const input = {
    preferredName: 'Claudine',
    altNames: { add: ['Bubbles'] },
    legalName: 'Claudine Eriepa',
    authors: {
      add: ['*']
    },
    recps: [groupId]
  }

  const profileId = await p(kaitiaki.ssb.profile.person.group.create)(input)

  const profileDetails = {
    preferredName: 'Cherese',
    legalName: 'Cherese Eriepa',
    gender: 'female',
    aliveInterval: '199X/',
    birthOrder: 2,
    authors: {
      add: ['*']
    },
    recps: [groupId]
  }

  // 2. member creates a submission to create a profile
  const submissionIdA = await proposeNewGroupPerson({
    input: profileDetails,
    groupId,
    comment: 'Create a new profile'
  })

  const linkDetails = {
    type: 'link/profile-profile/child',
    parent: profileId,
    relationshipType: 'birth',
    recps: [groupId]
  }

  // 3. member creates a submission to link a profile to another profile in the group
  const submissionIdB = await proposeNewWhakapapaLink({
    input: linkDetails,
    groupId,
    comment: 'Create a link'
  })

  // 4. member links the two submissions together
  await createSubmissionsLink({
    parent: submissionIdA,
    child: submissionIdB,
    mappedDependencies: [
      {
        missingField: 'child',
        replacementField: 'targetId'
      }
    ]
  })

  // 5. member can query all submissions
  submissions = await getSubmissionsMember()

  const expectedSubmissions = [{
    id: submissionIdA,
    details: {
      preferredName: 'Cherese'
    },
    dependencies: [{
      id: submissionIdB,
      details: {
        parent: profileId,
        relationshipType: 'birth'
      },
      sourceRecord: null,
      targetRecord: null
    }]
  }]

  t.deepEqual(
    submissions,
    expectedSubmissions,
    'member can get all submissions'
  )

  // 6. member replicates to the kaitiaki
  await member.replicate(kaitiaki)

  // 7. kaitiaki can query all submissions
  submissions = await getSubmissionsKaitiaiki()

  t.deepEqual(
    submissions,
    expectedSubmissions,
    'kaitiaki can get all submissions'
  )

  // 8. kaitiaki executes approves the submission
  //  NOTE: here we copy what its doing in the frontend
  const profileInput = {
    type: 'person',
    ...profileDetails
  }

  // here we manually execute the submission (carry out what the submission was proposing to do)
  const newProfileId = await savePerson(profileInput)

  await approveSubmission({
    id: submissionIdA,
    comment: 'OK',
    targetId: newProfileId
  })

  // 9. kaitiaki executes approves the dependent submission
  await approveNewWhakapapaLinkSubmission({
    id: submissionIdB,
    comment: 'creating link',
    allowedFields: linkDetails
  })

  // 10. kaitiaki queries the submissions
  submissions = await getSubmissionsKaitiaiki()

  const expectedUpdatedSubmissions = [{
    id: submissionIdA,
    details: {
      preferredName: 'Cherese'
    },
    dependencies: [{
      id: submissionIdB,
      details: {
        parent: profileId,
        relationshipType: 'birth'
      },
      sourceRecord: null,
      targetRecord: {
        type: 'link/profile-profile/child',
        parent: profileId,
        child: newProfileId,
        relationshipType: 'birth',
        legallyAdopted: null,
        recps: [groupId]
      }
    }]
  }]

  t.deepEqual(
    submissions,
    expectedUpdatedSubmissions,
    'member can get all submissions after approval'
  )

  // 11. kaitiaki replicates to the member
  await kaitiaki.replicate(member)

  // 12. member queries the submissions
  submissions = await getSubmissionsMember()

  t.deepEqual(
    submissions,
    expectedUpdatedSubmissions,
    'kaitiaki can get all submissions after approval'
  )
})
