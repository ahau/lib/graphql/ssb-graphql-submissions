const test = require('tape')
const { isMsgId } = require('ssb-ref')
const { promisify: p } = require('util')

const TestGroup = require('../../test-group')

const { handleErr, GetSubmission, PROPOSE_NEW_WHAKAPAPA_LINK } = require('../../lib/helpers')

const query = `
  query ($id: String!) {
    getSubmission (id: $id) {
      id
      sourceId
      targetId
      targetType

      recps
      groupId

      approvedByIds
      rejectedByIds
      ...on SubmissionWhakapapaLink {
        details {
          parent
          child
          relationshipType
          legallyAdopted
          recps
        }
        sourceRecord {
          linkId
          type
          parent
          child
          relationshipType
          legallyAdopted
          recps
        }
        targetRecord {
          linkId
          type
          parent
          child
          relationshipType
          legallyAdopted
          recps
        }
      }
      comments {
        authorId
        author {
          id
        }
        comment
      }
      tombstone {
        date
        reason
      }
    }
  }
`

test('approve new link/profile-profile/child', async (t) => {
  t.plan(18)

  const {
    kaitiaki,
    member,
    groupId,
    poBoxId
  } = await TestGroup()

  kaitiaki.getSubmission = GetSubmission(kaitiaki, t, query)
  member.getSubmission = GetSubmission(member, t, query)

  t.teardown(() => {
    kaitiaki.close()
    member.close()
  })

  let res
  let submission

  // member creates two profiles to link to
  const mumsProfileId = await p(member.ssb.profile.person.group.create)({
    preferredName: 'Claudine',
    authors: {
      add: ['*']
    },
    recps: [groupId]
  })
  t.true(isMsgId(mumsProfileId), 'creates a profile for the mother')

  const daughtersProfileId = await p(member.ssb.profile.person.group.create)({
    preferredName: 'Cherese',
    authors: {
      add: ['*']
    },
    recps: [groupId]
  })
  t.true(isMsgId(daughtersProfileId), 'creates a profile for the mother')

  const linkDetails = {
    parent: mumsProfileId,
    child: daughtersProfileId,
    relationshipType: 'birth'
  }

  // create a proposal for a new link
  res = await member.apollo.mutate({
    mutation: PROPOSE_NEW_WHAKAPAPA_LINK,
    variables: {
      input: {
        type: 'link/profile-profile/child',
        ...linkDetails
      },
      groupId
    }
  })
    .catch(handleErr)

  t.error(res.errors, 'proposeNewWhakapapaLink without error')

  const submissionId = res.data.proposeNewWhakapapaLink
  t.true(isMsgId(submissionId), 'proposeNewWhakapapaLink returned msgId for the new submission')
  t.error(res.errors, 'proposeNewWhakapapaLink profile without error')

  await member.replicate(kaitiaki)

  let expected = {
    id: submissionId,
    sourceId: null,
    targetId: null,
    targetType: 'link/profile-profile/child',
    groupId,
    approvedByIds: [],
    rejectedByIds: [],
    details: {
      ...linkDetails,
      legallyAdopted: null,
      recps: [groupId]
    },
    comments: [],
    sourceRecord: null,
    targetRecord: null,
    tombstone: null,
    recps: [poBoxId, member.id]
  }

  // 4. the member can query their submission
  submission = await member.getSubmission(submissionId)

  t.deepEqual(
    submission,
    expected,
    'member could query the submission'
  )

  // 5. the kaitiaki can query the members submission
  submission = await kaitiaki.getSubmission(submissionId)

  t.deepEqual(
    submission,
    expected,
    'kaitiaki could query the submission'
  )

  // 6. the kaitiaki can approve the members submission
  // member publishes the new profile
  res = await kaitiaki.apollo.mutate({
    mutation: `
      mutation ($id: String!, $comment: String, $allowedFields: LinkInput!) {
        approveNewWhakapapaLink(id: $id, comment: $comment, allowedFields: $allowedFields)
      }
      `,
    variables: {
      id: submissionId,
      comment: 'Yeah sure',
      allowedFields: {
        ...linkDetails,
        recps: [groupId]
      }
    }
  })
    .catch(handleErr)

  await kaitiaki.replicate(member)

  t.true(isMsgId(res.data.approveNewWhakapapaLink), 'approveNewWhakapapaLink returns a msgId')

  // 7. the kaitiaki sees the submission was approved
  submission = await kaitiaki.getSubmission(submissionId)

  t.true(isMsgId(submission.targetId), 'returns a targetId')
  t.true(isMsgId(submission.targetRecord.linkId), 'returns a target.linkId')
  t.equals(submission.targetId, submission.targetRecord.linkId, 'targetId and linkId are the same')

  expected = {
    id: submissionId,
    sourceId: null,
    targetId: submission.targetId, // hack because we dont know this
    targetType: 'link/profile-profile/child',
    groupId,
    approvedByIds: [kaitiaki.id],
    rejectedByIds: [],
    details: {
      ...linkDetails,
      legallyAdopted: null,
      recps: [groupId]
    },
    sourceRecord: null,
    targetRecord: {
      linkId: submission.targetRecord.linkId, // hack because we dont know this
      type: 'link/profile-profile/child',
      ...linkDetails,
      legallyAdopted: null,
      recps: [groupId]
    },
    comments: [
      {
        authorId: kaitiaki.id,
        author: null,
        comment: 'Yeah sure'
      }
    ],
    tombstone: null,
    recps: [poBoxId, member.id]
  }

  t.deepEqual(
    submission,
    expected,
    'kaitiaki could query the submission'
  )

  t.notEqual(submission.sourceId, submission.targetId, 'target and source ids are not the same')

  // 8. the member sees the submission was approved
  submission = await member.getSubmission(submissionId)

  t.deepEqual(
    submission,
    expected,
    'kaitiaki could query the submission'
  )
})
