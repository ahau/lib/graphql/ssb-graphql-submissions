const test = require('tape')
const { isMsgId } = require('ssb-ref')
const { promisify: p } = require('util')

const TestGroup = require('../../test-group')

const { handleErr, GetSubmission, PROPOSE_EDIT_WHAKAPAPA_LINK, APPROVE_EDIT_WHAKAPAPA_LINK_SUBMISSION } = require('../../lib/helpers')

const query = `
  query ($id: String!) {
    getSubmission (id: $id) {
      id
      sourceId
      targetId
      targetType

      groupId

      approvedByIds
      rejectedByIds
      ...on SubmissionWhakapapaLink {
        details {
          parent
          child
          relationshipType
          legallyAdopted
        }
        sourceRecord {
          linkId
          parent
          child
          relationshipType
          legallyAdopted
        }
        targetRecord {
          linkId
          parent
          child
          relationshipType
          legallyAdopted
        }
      }
      tombstone {
        date
        reason
      }
    }
  }
`

test('proposeEdit, get, approve link/profile-profile/child submission', async (t) => {
  t.plan(13)

  const {
    kaitiaki,
    member,
    groupId
  } = await TestGroup()

  kaitiaki.getSubmission = GetSubmission(kaitiaki, t, query)
  member.getSubmission = GetSubmission(member, t, query)

  t.teardown(() => {
    kaitiaki.close()
    member.close()
  })

  let res
  let submission
  let expected

  // 2. create test profiles
  const mumsProfileId = await p(kaitiaki.ssb.profile.person.group.create)({
    preferredName: 'Claudine',
    authors: {
      add: ['*']
    },
    recps: [groupId]
  })
  t.true(isMsgId(mumsProfileId), 'creates a profile for the mother')

  const daughtersProfileId = await p(kaitiaki.ssb.profile.person.group.create)({
    preferredName: 'Cherese',
    authors: {
      add: ['*']
    },
    recps: [groupId]
  })
  t.true(isMsgId(daughtersProfileId), 'creates a profile for the mother')

  // 2. create a whakapapa link between them
  const linkId = await p(kaitiaki.ssb.whakapapa.link.create)({
    type: 'link/profile-profile/child',
    parent: mumsProfileId,
    child: daughtersProfileId
  }, { relationshipType: 'whangai' })

  await kaitiaki.replicate(member)

  // 3. member creates a submission to edit the link
  const updateDetails = {
    relationshipType: 'birth'
  }

  res = await member.apollo.mutate({
    mutation: PROPOSE_EDIT_WHAKAPAPA_LINK,
    variables: {
      linkId,
      input: updateDetails,
      comment: 'Please update this to birth',
      groupId
    }
  })
    .catch(handleErr)

  t.error(res.errors, 'proposeEditWhakapapaLink without error')

  const submissionId = res.data.proposeEditWhakapapaLink
  t.true(isMsgId(submissionId), 'proposeEditWhakapapaLink returned msgId for the new submission')

  await member.replicate(kaitiaki)

  // 4. the member can query their submission
  submission = await member.getSubmission(submissionId)

  expected = {
    id: submissionId,
    sourceId: linkId,
    targetId: null,
    targetType: 'link/profile-profile/child',
    groupId,
    approvedByIds: [],
    rejectedByIds: [],
    details: {
      // NOTE: these are empty because they werent updated
      parent: null,
      child: null,
      relationshipType: 'birth',
      legallyAdopted: null
    },
    sourceRecord: {
      linkId,
      parent: mumsProfileId,
      child: daughtersProfileId,
      relationshipType: 'whangai',
      legallyAdopted: null
    },
    targetRecord: null,
    tombstone: null
  }

  t.deepEqual(
    submission,
    expected,
    'member could query the submission'
  )

  // 5. the kaitiaki can query the members submission
  submission = await kaitiaki.getSubmission(submissionId)

  t.deepEqual(
    submission,
    expected,
    'kaitiaki could query the submission'
  )

  // 6. the kaitiaki can manually approve the members submission
  const updateId = await p(kaitiaki.ssb.whakapapa.link.update)(linkId, updateDetails)

  res = await kaitiaki.apollo.mutate({
    mutation: `
      mutation ($id: String!, $targetId: String, $comment: String) {
        approveSubmission(id: $id, targetId: $targetId, comment: $comment)
      }
      `,
    variables: {
      id: submissionId,
      targetId: updateId,
      comment: 'Yeah sure, all approved'
    }
  })
    .catch(handleErr)

  await kaitiaki.replicate(member)

  t.true(isMsgId(res.data.approveSubmission), 'approveSubmission returns a msgId')

  // 7. the kaitiaki sees the submission was approved
  submission = await kaitiaki.getSubmission(submissionId)

  expected = {
    id: submissionId,
    sourceId: linkId,
    targetId: updateId,
    targetType: 'link/profile-profile/child',
    groupId,
    approvedByIds: [kaitiaki.id],
    rejectedByIds: [],
    details: {
      parent: null,
      child: null,
      relationshipType: 'birth',
      legallyAdopted: null
    },
    sourceRecord: {
      // note: this reflects the updated version too
      // because it doesnt return at that particular point
      linkId,
      parent: mumsProfileId,
      child: daughtersProfileId,
      relationshipType: 'birth',
      legallyAdopted: null
    },
    targetRecord: {
      linkId: updateId,
      // note: this only reflects the message
      // at that particular point of the update
      // so these fields are empty
      parent: null,
      child: null,
      relationshipType: 'birth',
      legallyAdopted: null
    },
    tombstone: null
  }

  t.deepEqual(
    submission,
    expected,
    'kaitiaki could query the submission'
  )

  // 8. the member sees the submission was approved
  submission = await member.getSubmission(submissionId)

  t.deepEqual(
    submission,
    expected,
    'kaitiaki could query the submission'
  )
})

test('proposeEdit, get, approve (with approveAndExecute) link/profile-profile/child submission', async (t) => {
  t.plan(13)

  const {
    kaitiaki,
    member,
    groupId
  } = await TestGroup()

  kaitiaki.getSubmission = GetSubmission(kaitiaki, t, query)
  member.getSubmission = GetSubmission(member, t, query)

  t.teardown(() => {
    kaitiaki.close()
    member.close()
  })

  let res
  let submission
  let expected

  // 2. create test profiles
  const mumsProfileId = await p(kaitiaki.ssb.profile.person.group.create)({
    preferredName: 'Claudine',
    authors: {
      add: ['*']
    },
    recps: [groupId]
  })
  t.true(isMsgId(mumsProfileId), 'creates a profile for the mother')

  const daughtersProfileId = await p(kaitiaki.ssb.profile.person.group.create)({
    preferredName: 'Cherese',
    authors: {
      add: ['*']
    },
    recps: [groupId]
  })
  t.true(isMsgId(daughtersProfileId), 'creates a profile for the mother')

  // 2. create a whakapapa link between them
  const linkId = await p(kaitiaki.ssb.whakapapa.link.create)({
    type: 'link/profile-profile/child',
    parent: mumsProfileId,
    child: daughtersProfileId
  }, { relationshipType: 'whangai' })

  await kaitiaki.replicate(member)

  // 3. member creates a submission to edit the link
  const updateDetails = {
    relationshipType: 'birth'
  }

  res = await member.apollo.mutate({
    mutation: PROPOSE_EDIT_WHAKAPAPA_LINK,
    variables: {
      linkId,
      input: updateDetails,
      comment: 'Please update this to birth',
      groupId
    }
  })
    .catch(handleErr)

  t.error(res.errors, 'proposeEditWhakapapaLink without error')

  const submissionId = res.data.proposeEditWhakapapaLink
  t.true(isMsgId(submissionId), 'proposeEditWhakapapaLink returned msgId for the new submission')

  await member.replicate(kaitiaki)

  // 4. the member can query their submission
  submission = await member.getSubmission(submissionId)

  expected = {
    id: submissionId,
    sourceId: linkId,
    targetId: null,
    targetType: 'link/profile-profile/child',
    groupId,
    approvedByIds: [],
    rejectedByIds: [],
    details: {
      // NOTE: these are empty because they werent updated
      parent: null,
      child: null,
      relationshipType: 'birth',
      legallyAdopted: null
    },
    sourceRecord: {
      linkId,
      parent: mumsProfileId,
      child: daughtersProfileId,
      relationshipType: 'whangai',
      legallyAdopted: null
    },
    targetRecord: null,
    tombstone: null
  }

  t.deepEqual(
    submission,
    expected,
    'member could query the submission'
  )

  // 5. the kaitiaki can query the members submission
  submission = await kaitiaki.getSubmission(submissionId)

  t.deepEqual(
    submission,
    expected,
    'kaitiaki could query the submission'
  )

  // 6. the kaitiaki can approve and execute the members submission
  res = await kaitiaki.apollo.mutate({
    mutation: APPROVE_EDIT_WHAKAPAPA_LINK_SUBMISSION,
    variables: {
      id: submissionId,
      allowedFields: updateDetails,
      comment: 'Yeah sure, all approved'
    }
  })
    .catch(handleErr)

  await kaitiaki.replicate(member)

  t.true(isMsgId(res.data.approveEditWhakapapaLink), 'approveEditWhakapapaLink returns a msgId')

  // 7. the kaitiaki sees the submission was approved
  submission = await kaitiaki.getSubmission(submissionId)

  expected = {
    id: submissionId,
    sourceId: linkId,
    targetId: submission.targetId,
    targetType: 'link/profile-profile/child',
    groupId,
    approvedByIds: [kaitiaki.id],
    rejectedByIds: [],
    details: {
      parent: null,
      child: null,
      relationshipType: 'birth',
      legallyAdopted: null
    },
    sourceRecord: {
      // note: this reflects the updated version too
      // because it doesnt return at that particular point
      linkId,
      parent: mumsProfileId,
      child: daughtersProfileId,
      relationshipType: 'birth',
      legallyAdopted: null
    },
    targetRecord: {
      linkId: submission.targetId,
      // note: this only reflects the message
      // at that particular point of the update
      // so these fields are empty
      parent: null,
      child: null,
      relationshipType: 'birth',
      legallyAdopted: null
    },
    tombstone: null
  }

  t.deepEqual(
    submission,
    expected,
    'kaitiaki could query the submission'
  )

  // 8. the member sees the submission was approved
  submission = await member.getSubmission(submissionId)

  t.deepEqual(
    submission,
    expected,
    'kaitiaki could query the submission'
  )
})

test('proposeEdit, get, reject link/profile-profile/child submission', async (t) => {
  t.plan(13)

  const {
    kaitiaki,
    member,
    groupId
  } = await TestGroup()

  kaitiaki.getSubmission = GetSubmission(kaitiaki, t, query)
  member.getSubmission = GetSubmission(member, t, query)

  t.teardown(() => {
    kaitiaki.close()
    member.close()
  })

  let res
  let submission
  let expected

  // 2. create test profiles
  const mumsProfileId = await p(kaitiaki.ssb.profile.person.group.create)({
    preferredName: 'Claudine',
    authors: {
      add: ['*']
    },
    recps: [groupId]
  })
  t.true(isMsgId(mumsProfileId), 'creates a profile for the mother')

  const daughtersProfileId = await p(kaitiaki.ssb.profile.person.group.create)({
    preferredName: 'Cherese',
    authors: {
      add: ['*']
    },
    recps: [groupId]
  })
  t.true(isMsgId(daughtersProfileId), 'creates a profile for the mother')

  // 2. create a whakapapa link between them
  const linkId = await p(kaitiaki.ssb.whakapapa.link.create)({
    type: 'link/profile-profile/child',
    parent: mumsProfileId,
    child: daughtersProfileId
  }, { relationshipType: 'whangai' })

  await kaitiaki.replicate(member)

  // 3. member creates a submission to edit the link
  const updateDetails = {
    relationshipType: 'birth'
  }

  res = await member.apollo.mutate({
    mutation: PROPOSE_EDIT_WHAKAPAPA_LINK,
    variables: {
      linkId,
      input: updateDetails,
      comment: 'Please update this to birth',
      groupId
    }
  })
    .catch(handleErr)

  t.error(res.errors, 'proposeEditWhakapapaLink without error')

  const submissionId = res.data.proposeEditWhakapapaLink
  t.true(isMsgId(submissionId), 'proposeEditWhakapapaLink returned msgId for the new submission')

  await member.replicate(kaitiaki)

  // 4. the member can query their submission
  submission = await member.getSubmission(submissionId)

  expected = {
    id: submissionId,
    sourceId: linkId,
    targetId: null,
    targetType: 'link/profile-profile/child',
    groupId,
    approvedByIds: [],
    rejectedByIds: [],
    details: {
      // NOTE: these are empty because they werent updated
      parent: null,
      child: null,
      relationshipType: 'birth',
      legallyAdopted: null
    },
    sourceRecord: {
      linkId,
      parent: mumsProfileId,
      child: daughtersProfileId,
      relationshipType: 'whangai',
      legallyAdopted: null
    },
    targetRecord: null,
    tombstone: null
  }

  t.deepEqual(
    submission,
    expected,
    'member could query the submission'
  )

  // 5. the kaitiaki can query the members submission
  submission = await kaitiaki.getSubmission(submissionId)

  t.deepEqual(
    submission,
    expected,
    'kaitiaki could query the submission'
  )

  // 6. the kaitiaki can reject the members submission
  res = await kaitiaki.apollo.mutate({
    mutation: `
      mutation ($id: String!, $comment: String) {
        rejectSubmission(id: $id, comment: $comment)
      }
      `,
    variables: {
      id: submissionId,
      comment: 'Cannot verify these changes'
    }
  })
    .catch(handleErr)

  await kaitiaki.replicate(member)

  t.true(isMsgId(res.data.rejectSubmission), 'rejectSubmission returns a msgId')

  // 7. the kaitiaki sees the submission was rejected
  submission = await kaitiaki.getSubmission(submissionId)

  expected = {
    id: submissionId,
    sourceId: linkId,
    targetId: null,
    targetType: 'link/profile-profile/child',
    groupId,
    approvedByIds: [],
    rejectedByIds: [kaitiaki.id],
    details: {
      parent: null,
      child: null,
      relationshipType: 'birth',
      legallyAdopted: null
    },
    sourceRecord: {
      linkId,
      parent: mumsProfileId,
      child: daughtersProfileId,
      relationshipType: 'whangai',
      legallyAdopted: null
    },
    targetRecord: null,
    tombstone: null
  }

  t.deepEqual(
    submission,
    expected,
    'kaitiaki could query the submission'
  )

  // 8. the member sees the submission was approved
  submission = await member.getSubmission(submissionId)

  t.deepEqual(
    submission,
    expected,
    'kaitiaki could query the submission'
  )
})
