const test = require('tape')
const { isMsgId } = require('ssb-ref')
const { promisify: p } = require('util')

const TestGroup = require('../../test-group')
const { PROPOSE_NEW_WHAKAPAPA_LINK, handleErr, PROPOSE_NEW_MUTATION, GetSubmissions } = require('../../lib/helpers')

function CreateSubmissionLink (testbot) {
  return function ({ parent, child, mappedDependencies = [] }) {
    return testbot.apollo.mutate({
      mutation: `
        mutation ($parent: String!, $child: String!, $mappedDependencies: [MappedDependencyInput]) {
          createSubmissionsLink (parent: $parent, child: $child, mappedDependencies: $mappedDependencies)
        }
      `,
      variables: {
        parent,
        child,
        mappedDependencies
      }
    })
  }
}

function GetSubmissionLink (testbot) {
  return function (id) {
    return testbot.apollo.query({
      query: `
        query ($id: String!) {
          getSubmissionsLink (id: $id) {
            id
            parent
            child
            mappedDependencies {
              missingField
              replacementField
            }
          }
        }
      `,
      variables: { id }
    })
  }
}

const submissionFragment = `
  fragment SubmissionFragment on Submission {
    id
    sourceId
    targetId
    targetType
    approvedByIds
    rejectedByIds
    ...on SubmissionGroupPerson {
      source
      details {
        preferredName
        legalName
        recps
        tombstone {
          date
          reason
        }
      }
    }
    ...on SubmissionWhakapapaLink {
      details {
        parent
        child
        relationshipType
        legallyAdopted
      }
    }

    comments {
      authorId
      comment
    }
    tombstone {
      date
      reason
    }
  }
`

const getSubmissionsQuery = `
  ${submissionFragment}
  query {
    getSubmissions {
      ...SubmissionFragment,
      dependencies {
        ...SubmissionFragment
      }  
    }
  }
`

test('createSubmissionLink (with mapped dependencies)', async (t) => {
  t.plan(16)

  const {
    kaitiaki,
    member,
    groupId
  } = await TestGroup()

  t.teardown(() => {
    kaitiaki.close()
    member.close()
  })

  const createSubmissionLink = CreateSubmissionLink(member)
  const getSubmissionLink = GetSubmissionLink(member)
  const getSubmissions = GetSubmissions(member, t, getSubmissionsQuery)

  let res

  // 1. kaitiaki creates the profile which will be the "parent"
  const mumsProfileId = await p(member.ssb.profile.person.group.create)({
    preferredName: 'Claudine',
    authors: {
      add: ['*']
    },
    recps: [groupId]
  })
  t.true(isMsgId(mumsProfileId), 'creates a profile for the mother')

  // 2. member create two submissions to create + link a profile

  // create the proposal for the profile
  res = await member.apollo.mutate({
    mutation: PROPOSE_NEW_MUTATION,
    variables: {
      input: {
        preferredName: 'Cherese'
      },
      comment: 'Can i please make this profile for my daughter?',
      groupId
    }
  })
    .catch(handleErr)

  t.error(res.errors, 'proposeNewWhakapapaLink without error')
  const submissionIdA = res.data.proposeNewGroupPerson
  t.true(isMsgId(submissionIdA), 'proposeNewGroupPerson returned msgId for the new submission')

  // create the proposal for the link
  const whakapapaLinkDetails = {
    parent: mumsProfileId,
    // child missing here - this is a mapped dependency
    relationshipType: 'birth'
  }

  res = await member.apollo.mutate({
    mutation: PROPOSE_NEW_WHAKAPAPA_LINK,
    variables: {
      input: {
        type: 'link/profile-profile/child',
        ...whakapapaLinkDetails
      },
      comment: 'Can I please link my daughter to me?',
      groupId
    }
  })
    .catch(handleErr)

  t.error(res.errors, 'proposeNewWhakapapaLink without error')

  const submissionIdB = res.data.proposeNewWhakapapaLink
  t.true(isMsgId(submissionIdB), 'proposeNewWhakapapaLink returned msgId for the new submission')

  // 3. link the two submissions together and specifiy which fields need to be mapped
  res = await createSubmissionLink({
    parent: submissionIdA,
    child: submissionIdB,
    mappedDependencies: [{
      missingField: 'child',
      replacementField: 'targetId'
    }]
  })

  t.error(res.errors, 'createSubmissionsLink without error')
  const submissionsLinkId = res.data.createSubmissionsLink
  t.true(isMsgId(submissionsLinkId), 'createSubmissionsLink returns a linkId')

  res = await getSubmissionLink(submissionsLinkId)
  t.error(res.errors, 'getSubmissionsLink returns without error')
  const submissionsLink = res.data.getSubmissionsLink

  t.deepEqual(
    submissionsLink,
    {
      id: submissionsLinkId,
      parent: submissionIdA,
      child: submissionIdB,
      mappedDependencies: [
        {
          missingField: 'child',
          replacementField: 'targetId'
        }
      ]
    },
    'returns the correct link information'
  )

  res = await getSubmissions()

  t.error(res.errors, 'getSubmissions returns without error')

  const parentSubmission = {
    id: submissionIdA,
    sourceId: null,
    targetId: null,
    targetType: 'profile/person/admin',
    approvedByIds: [],
    rejectedByIds: [],
    source: 'ahau',
    details: {
      preferredName: 'Cherese',
      legalName: null,
      recps: [groupId],
      tombstone: null
    },
    comments: [
      {
        authorId: member.id,
        comment: 'Can i please make this profile for my daughter?'
      }
    ],
    tombstone: null
  }

  t.deepEqual(
    res,
    [{
      ...parentSubmission,
      dependencies: [
        {
          id: submissionIdB,
          sourceId: null,
          targetId: null,
          targetType: 'link/profile-profile/child',
          approvedByIds: [],
          rejectedByIds: [],
          details: {
            parent: mumsProfileId,
            child: null, // TO BE APPLIED DURING APPROVAL PROCESS
            relationshipType: 'birth',
            legallyAdopted: null
          },
          comments: [
            {
              authorId: member.id,
              comment: 'Can I please link my daughter to me?'
            }
          ],
          tombstone: null
        }
      ]
    }],
    'returns dependant submissions'
  )

  // tombstone a linked submission
  await p(member.ssb.submissions.tombstone)(submissionIdB, { reason: 'deleted' })

  res = await getSubmissions()

  t.deepEqual(
    res,
    [{
      ...parentSubmission,
      dependencies: []
    }]
  )

  await p(member.ssb.submissions.tombstone)(submissionIdA, { reason: 'deleted' })

  res = await getSubmissions()

  t.deepEqual(res, [], 'tombstoned parent submission was filtered out')
})
