const { promisify: p } = require('util')
const TestBot = require('./test-bot')
const { sleep } = require('./lib/helpers')

module.exports = async function TestGroup (opts = {}) {
  const kaitiaki = await TestBot({ name: 'kaitiaki', tribes: true, ...opts })
  const member = await TestBot({ name: 'member', tribes: true, ...opts })

  if (opts.makeProfiles) {
    // for displaying who approved or commented
    const kaitiakiPublicProfileId = await p(kaitiaki.ssb.profile.person.public.create)({ preferredName: 'Kaitiaki', authors: { add: [kaitiaki.id] } })
    await p(kaitiaki.ssb.profile.link.create)(kaitiakiPublicProfileId)
    const memberPublicProfileId = await p(member.ssb.profile.person.public.create)({ preferredName: 'Member', authors: { add: [member.id] } })
    await p(member.ssb.profile.link.create)(memberPublicProfileId)
  }

  // kaitiaki sets up the group
  const { groupId, poBoxId } = await p(kaitiaki.ssb.tribes.create)({ addPOBox: true })

  // kaitiaki adds the member to the group
  await p(kaitiaki.ssb.tribes.invite)(groupId, [member.id], {})

  await kaitiaki.replicate(member)
  await member.replicate(kaitiaki)

  // wait for rebuild
  await sleep(500)

  return {
    kaitiaki,
    member,
    groupId,
    poBoxId
  }
}
