/**
 *
 * EXTRAS
 *
 */
function sleep (ms) {
  return new Promise(resolve => setTimeout(resolve, ms))
}

function handleErr (err, t) {
  console.log(JSON.stringify(err, null, 2))
  return { errors: [err] }
}

let i = 0

const generateTimestamp = () => {
  return (Date.now() + (i += 10)).toString()
}

/**
 *
 * QUERIES
 *
 */

const GET_SUBMISSION = `
  query ($id: String!) {
    getSubmission (id: $id) {
      id
      sourceId
      targetId
      targetType

      groupId

      approvedByIds
      rejectedByIds
      ...on SubmissionGroupPerson {
        source
        details {
          preferredName
          legalName
          recps
          tombstone {
            date
            reason
          }
        }
      }
      comments {
        authorId
        author {
          id
        }
        comment
      }
      tombstone {
        date
        reason
      }
    }
  }
`

const GET_SUBMISSIONS = `
  query {
    getSubmissions {
      id
      sourceId
      targetId
      targetType
      approvedByIds
      rejectedByIds
      ...on SubmissionGroupPerson {
        source
        details {
          preferredName
          legalName
          recps
          tombstone {
            date
            reason
          }
        }
      }
      comments {
        authorId
        comment
      }
      tombstone {
        date
        reason
      }
    }
  }
`

/**
 *
 * MUTATIONS
 *
 */
const PROPOSE_EDIT_MUTATION = `
  mutation (
    $profileId: String!
    $input: PersonProfileInput!
    $comment: String
    $groupId: String!
  ) {
    proposeEditGroupPerson (
      profileId: $profileId
      input: $input
      comment: $comment
      groupId: $groupId
    )
  }
`

const PROPOSE_NEW_MUTATION = `
  mutation (
    $input: PersonProfileInput!
    $comment: String
    $groupId: String!
    $source: SubmissionSource
    $joiningQuestions: [SubmissionJoiningQuestionInput]
  ) {
    proposeNewGroupPerson (
      input: $input
      comment: $comment
      groupId: $groupId
      source: $source
      joiningQuestions: $joiningQuestions
    )
  }
`

const PROPOSE_TOMBSTONE_MUTATION = `
  mutation (
    $recordId: String!
    $comment: String
    $groupId: String!
  ) {
    proposeTombstone (
      recordId: $recordId
      comment: $comment
      groupId: $groupId
    )
  }
`

const PROPOSE_NEW_WHAKAPAPA_LINK = `
  mutation (
    $input: LinkInput!
    $comment: String
    $groupId: String!
  ) {
    proposeNewWhakapapaLink (
      input: $input
      comment: $comment
      groupId: $groupId
    )
  }
`

const PROPOSE_EDIT_WHAKAPAPA_LINK = `
  mutation (
    $linkId: String!
    $input: LinkInput!
    $comment: String
    $groupId: String!
  ) {
    proposeEditWhakapapaLink (
      linkId: $linkId
      input: $input
      comment: $comment
      groupId: $groupId
    )
  }
`

const APPROVE_SUBMISSION = `
  mutation (
    $id: String! # submissionId
    $comment: String
    $targetId: String
  ) {
    approveSubmission (
      id: $id
      comment: $comment
      targetId: $targetId
    )
  }
`

const APPROVE_WHAKAPAPA_LINK_SUBMISSION = `
  mutation (
    $id: String!
    $comment: String
    $allowedFields: LinkInput!
  ) {
    approveNewWhakapapaLink (
      id: $id
      comment: $comment
      allowedFields: $allowedFields
    )
  }
`

const APPROVE_EDIT_WHAKAPAPA_LINK_SUBMISSION = `
  mutation (
    $id: String!
    $comment: String
    $allowedFields: LinkInput!
  ) {
    approveEditWhakapapaLink (
      id: $id
      comment: $comment
      allowedFields: $allowedFields
    )
  }
`

const PROPOSE_EDIT_WHAKAPAPA_VIEW_MUTATION = `
  mutation (
    $whakapapaId: String!
    $input: WhakapapaViewInput!
    $comment: String
    $groupId: String!
  ) {
    proposeEditWhakapapaView (
      whakapapaId: $whakapapaId
      input: $input
      comment: $comment
      groupId: $groupId
    )
  }
`

const APPROVE_EDIT_WHAKAPAPA_VIEW_MUTATION = `
  mutation (
    $id: String!
    $comment: String
    $allowedFields: WhakapapaViewInput!
  ) {
    approveEditWhakapapaView (
      id: $id
      comment: $comment
      allowedFields: $allowedFields
    )
  }
`

const CREATE_SUBMISSIONS_LINK = `
  mutation (
    $parent: String
    $child: String
    $mappedDependencies: [MappedDependencyInput]
  ) {
    createSubmissionsLink (
      parent: $parent
      child: $child
      mappedDependencies: $mappedDependencies
    )
  }
`

const SAVE_PERSON = `
  mutation($input: PersonProfileInput!) {
    savePerson(input: $input)
  }
`

/**
 *
 * QUERY HELPERS
 *
 */

function GetSubmission (testbot, t, query) {
  return async function getSubmission (id) {
    const res = await testbot.apollo.query({
      query: query || GET_SUBMISSION,
      variables: { id }
    })
      .catch(handleErr)

    t.error(res.errors, `${testbot.name || ''} can query the submission without error`)

    return res.data.getSubmission
  }
}

function GetSubmissions (testbot, t, query) {
  return async function getSubmissions () {
    const res = await testbot.apollo.query({
      query: query || GET_SUBMISSIONS
    })
      .catch(handleErr)

    t.error(res.errors, `${testbot.name || ''} can query the submission without error`)

    return res.data.getSubmissions
  }
}

/**
 *
 * MUTATION HELPERS
 *
 */

function ApproveSubmission (testbot, t) {
  return async function approveSubmission (input) {
    const res = await testbot.apollo.mutate({
      mutation: APPROVE_SUBMISSION,
      variables: input
    })
      .catch(handleErr)

    t.error(res.errors, `${testbot.name || ''} approved a profile submission`)
  }
}

function ApproveNewWhakapapaLinkSubmission (testbot, t) {
  return async function approveNewWhakapapaLinkSubmission (input) {
    const res = await testbot.apollo.mutate({
      mutation: APPROVE_WHAKAPAPA_LINK_SUBMISSION,
      variables: input
    })
      .catch(handleErr)

    t.error(res.errors, `${testbot.name || ''} approved a whakapapa link submission`)

    return res.data.approveNewWhakapapaLink
  }
}

function ApproveEditWhakapapaViewSubmission (testbot, t) {
  return async function approveEditWhakapapaViewSubmission (input) {
    const res = await testbot.apollo.mutate({
      mutation: APPROVE_EDIT_WHAKAPAPA_VIEW_MUTATION,
      variables: input
    })
      .catch(handleErr)

    t.error(res.errors, `${testbot.name || ''} approved a whakapapa view submission`)

    return res.data.approveWhakapapaViewSubmission
  }
}

function ProposeNewGroupPerson (testbot, t) {
  return async function proposeNewGroupPerson (input) {
    const res = await testbot.apollo.mutate({
      mutation: PROPOSE_NEW_MUTATION,
      variables: input
    })
      .catch(handleErr)

    t.error(res.errors, `${testbot.name || ''} proposed a new group person profile`)

    return res.data.proposeNewGroupPerson
  }
}

function ProposeEditWhakapapaView (testbot, t) {
  return async function proposeEditWhakapapaView (input) {
    const res = await testbot.apollo.mutate({
      mutation: PROPOSE_EDIT_WHAKAPAPA_VIEW_MUTATION,
      variables: input
    })
      .catch(handleErr)

    t.error(res.errors, `${testbot.name || ''} proposed an edit to a whakapapa view`)

    return res.data.proposeEditWhakapapaView
  }
}

function ProposeNewWhakapapaLink (testbot, t) {
  return async function proposeNewWhakapapaLink (input) {
    const res = await testbot.apollo.mutate({
      mutation: PROPOSE_NEW_WHAKAPAPA_LINK,
      variables: input
    })
      .catch(handleErr)

    t.error(res.errors, `${testbot.name || ''} proposed a new whakapapa link`)

    return res.data.proposeNewWhakapapaLink
  }
}

function CreateSubmissionsLink (testbot, t) {
  return async function createSubmissionsLink (input) {
    const res = await testbot.apollo.mutate({
      mutation: CREATE_SUBMISSIONS_LINK,
      variables: input
    })
      .catch(handleErr)

    t.error(res.errors, `${testbot.name || ''} linked two submissions together`)

    return res.data.createSubmissionsLink
  }
}

function SavePerson (testbot, t) {
  return async function savePerson (input) {
    const res = await testbot.apollo.mutate({
      mutation: SAVE_PERSON,
      variables: { input }
    })
      .catch(handleErr)

    t.error(res.errors, `${testbot.name || ''} saved a profile`)

    return res.data.savePerson
  }
}

module.exports = {
  // API
  GetSubmission,
  GetSubmissions,
  ApproveSubmission,
  ApproveNewWhakapapaLinkSubmission,
  ApproveEditWhakapapaViewSubmission,
  ProposeNewGroupPerson,
  ProposeNewWhakapapaLink,
  ProposeEditWhakapapaView,
  CreateSubmissionsLink,
  SavePerson,

  // MUTATIONS
  PROPOSE_NEW_MUTATION,
  PROPOSE_EDIT_MUTATION,
  PROPOSE_TOMBSTONE_MUTATION,
  PROPOSE_NEW_WHAKAPAPA_LINK,
  PROPOSE_EDIT_WHAKAPAPA_LINK,
  PROPOSE_EDIT_WHAKAPAPA_VIEW_MUTATION,
  APPROVE_EDIT_WHAKAPAPA_LINK_SUBMISSION,

  // extras
  handleErr,
  sleep,
  generateTimestamp
}
