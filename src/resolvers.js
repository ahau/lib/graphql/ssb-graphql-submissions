const { promisify: p } = require('util')
const { GraphQLError } = require('graphql')

const { person: fixPersonInput } = require('@ssb-graphql/profile/src/lib/fix-input')
const { link: fixLinkInput, whakapapaView: fixWhakapapaViewInput } = require('@ssb-graphql/whakapapa/src/fix-input')
const CrutByType = require('ssb-whakapapa/lib/crut-by-type')

const GetPoBoxForGroup = require('./lib/get-po-box-for-group')
const RenderUpdate = require('./lib/render-update')
const {
  CHILD_LINK,
  PARTNER_LINK,
  PERSON_ADMIN,
  PERSON,
  WHAKAPAPA,
  SUBMISSION_GROUP_PERSON,
  SUBMISSION_WHAKAPAPA_LINK,
  SUBMISSION_WHAKAPAPA_VIEW
} = require('./lib/constants')

// TODO: better error handling
// function handleErr (err) {
//   console.error(err)
// }

module.exports = (ssb, gettersWithCache) => {
  const crutByType = CrutByType(ssb)
  const getPoBoxForGroup = GetPoBoxForGroup(ssb)
  const renderUpdate = RenderUpdate(ssb)

  const { getProfile } = gettersWithCache
  const getProfileById = p(getProfile)

  ssb.submissions.registerHandler(ssb.profile.person.group)
  ssb.submissions.registerHandler(ssb.profile.person.admin)
  ssb.submissions.registerHandler(crutByType(CHILD_LINK))
  ssb.submissions.registerHandler(crutByType(PARTNER_LINK))
  ssb.submissions.registerHandler(ssb.whakapapa.view)

  function getSubmission (submissionId, cb) {
    ssb.submissions.get(submissionId, (err, submission) => {
      if (err) {
        // here we ignore the message if we couldnt get the submission
        // console.error(err.message)
        return cb(null, null)
      }

      cb(null, submission)
    })
  }

  function sharedSubmissionResolvers () {
    return {
      id: submission => submission.key,
      comments: submission => {
        // converts the comments from
        // { [authorId]: comment }
        // to
        // [{ authorId, comment }, ...]
        return Object.entries(submission.comments)
          .map(([authorId, comment]) => ({ authorId, comment }))
      },
      applicantId: submission => submission.originalAuthor,
      applicant: submission => getProfileById(submission.originalAuthor),
      group: async (submission) => {
        if (!submission.recps?.length) return

        const { groupId } = submission
        if (!groupId) return

        const group = await p(ssb.profile.findByGroupId)(groupId)

        const pubProfile = []
        for await (const profile of group.public) {
          const res = await getProfileById(profile.key)
          pubProfile.push(res)
        }
        const privProfile = []
        for await (const profile of group.private) {
          const res = await getProfileById(profile.key)
          privProfile.push(res)
        }
        return {
          id: groupId,
          public: pubProfile,
          private: privProfile
        }
      }
    }
  }

  function getLinkRecord (id, type) {
    if (!id || !type) return

    return new Promise((resolve, reject) => {
      const crut = crutByType(type)

      crut.read(id, (err, link) => {
        if (err) {
          // TODO cherese 2023-10-31 the links are sometimes failing for some reason, the error is about them
          // having authors field
          // this is just a work around until i have time to fix it properly
          return p(renderUpdate)(id)
            .then(resolve)
            .catch(reject)
        } else {
          link.linkId = id
          resolve(link)
        }
      })
    })
  }

  function getWhakapapaRecord (id) {
    if (!id) return

    return ssb.whakapapa.view.get(id)
  }

  return {
    resolvers: {
      Query: {
        getSubmission: (_, { id }) => ssb.submissions.get(id),
        getSubmissions: async (_, opts = {}) => {
          return ssb.submissions.list({
            read: (msg, cb) => {
              // NOTE: temp fix to a bug where msg is a string
              const submissionId = (typeof msg === 'object')
                ? msg.key
                : msg

              ssb.submissions.link.list({
                // filter out all links where this submission is the child
                // because we only want to keep the "parent" submissions
                filter: link => link.child === submissionId
              }, (err, links) => {
                if (err || links.length) return cb(null, null) // filtered out by ssb-crut

                getSubmission(submissionId, cb)
              })
            }
          })
        },
        getSubmissionsLink: (_, { id }) => ssb.submissions.link.get(id)
      },
      Mutation: {
        /**
         *
         * submissions base API
         *
         */
        approveSubmission: (_, { id, comment, targetId }) => p(ssb.submissions.approve)(id, { comment, targetId }),
        rejectSubmission: (_, { id, comment }) => p(ssb.submissions.reject)(id, { comment }),
        tombstoneSubmission: (_, { id, tombstoneInput }) => p(ssb.submissions.tombstone)(id, tombstoneInput),

        // universal method can be used to propose a tombstone across all types
        // NOTE: types must have registered a handler for the type
        proposeTombstone: async (_, { recordId, comment, groupId }) => {
          const recps = await getPoBoxForGroup(groupId)

          return p(ssb.submissions.proposeTombstone)(recordId, { comment, recps, groupId })
        },

        /**
         *
         * profile.person.group creates
         *
         */

        proposeNewGroupPerson: async (_, { input, comment, groupId, source, joiningQuestions }) => {
          const recps = await getPoBoxForGroup(groupId)

          if (!input.recps) {
            input.recps = [groupId]
          }

          const submissionDetails = {
            groupId,
            recps,
            source,
            comment
          }

          if (joiningQuestions) submissionDetails.moreInfo = { joiningQuestions }

          // const adminFields = pick(input, ['email', 'phone', 'address'])

          // if (Object.keys(adminFields).length) {
          return p(ssb.submissions.proposeNew)({ type: 'profile/person/admin' }, fixPersonInput(input), submissionDetails)
          // }

          // return p(ssb.submissions.proposeNew)({ type: 'profile/person' }, fixPersonInput(input), submissionDetails)
        },
        approveNewGroupPerson: async (_, { id, comment, allowedFields }) => {
          return p(ssb.submissions.executeAndApprove)(id, fixPersonInput(allowedFields), { comment })
        },

        /*
        profile.person.group edits
        */
        proposeEditGroupPerson: async (_, { profileId, input, comment, groupId }) => {
          const recps = await getPoBoxForGroup(groupId)

          if (input.recps) {
            console.warn('proposeEditGroupPerson found recps on input, cannot make a submission to change the recps field of a profile')
            delete input.recps
          }

          return p(ssb.submissions.proposeEdit)(profileId, fixPersonInput(input), { comment, groupId, recps })
        },
        approveEditGroupPerson: async (_, { id, comment, allowedFields }) => {
          return p(ssb.submissions.executeAndApprove)(id, fixPersonInput(allowedFields), { comment })
        },

        /**
         *
         *  whakapapa.link API
         *
         */
        proposeNewWhakapapaLink: async (_, { input, comment, groupId }) => {
          const { type, parent, child } = input
          delete input.type

          // NOTE: manually doing this check here because we are reusing types from
          //  @ssb-graphql/whakapapa for convenience
          if (!type) throw new GraphQLError('link.type is required and is missing from input')

          const recps = await getPoBoxForGroup(groupId)

          // NOTE: fixLinkInput actually removes the parent, child, recps from
          // the input
          const recordDetails = {
            parent,
            child,
            ...fixLinkInput(input)
          }

          if (!recordDetails.recps) {
            recordDetails.recps = [groupId]
          }

          return p(ssb.submissions.proposeNew)({ type }, recordDetails, { comment, groupId, recps })
        },
        proposeEditWhakapapaLink: async (_, { linkId, input, comment, groupId }) => {
          const recps = await getPoBoxForGroup(groupId)

          if (input.recps) {
            console.warn('proposeEditWhakapapaLink found recps on input, cannot make a submission to change the recps field of a link')
            delete input.recps
          }

          return p(ssb.submissions.proposeEdit)(linkId, fixLinkInput(input), { comment, groupId, recps })
        },
        approveNewWhakapapaLink: async (_, { id, comment, allowedFields }) => {
          const { child, parent } = allowedFields
          const approvedFields = {
            child,
            parent,
            ...fixLinkInput(allowedFields)
          }

          return p(ssb.submissions.executeAndApprove)(id, approvedFields, { comment })
        },
        approveEditWhakapapaLink: async (_, { id, comment, allowedFields }) => {
          return p(ssb.submissions.executeAndApprove)(id, fixLinkInput(allowedFields), { comment })
        },
        /**
         *
         *  whakapapaView API
         *
         */

        // TODO: proposeNewWhakapapaView

        proposeEditWhakapapaView: async (_, { whakapapaId, input, comment, groupId }) => {
          const recps = await getPoBoxForGroup(groupId)

          if (input.recps) {
            console.warn('proposeEditWhakapapaView found recps on input, cannot make a submission to change the recps field of a whakapapaView')
            delete input.recps
          }

          return p(ssb.submissions.proposeEdit)(whakapapaId, fixWhakapapaViewInput(input), { comment, groupId, recps })
        },
        approveEditWhakapapaView: async (_, { id, comment, allowedFields }) => {
          return p(ssb.submissions.executeAndApprove)(id, fixWhakapapaViewInput(allowedFields), { comment })
        },

        /**
         *
         *  submissions.link API
         *
         */
        createSubmissionsLink: async (_, { parent, child, mappedDependencies }) => {
          return p(ssb.submissions.link.create)(parent, child, { mappedDependencyFields: convertMapping(mappedDependencies) })
        }
      },
      Submission: {
        __resolveType (submission) {
          switch (submission.targetType) {
            case PERSON_ADMIN:
            case PERSON:
              return SUBMISSION_GROUP_PERSON
            case CHILD_LINK:
            case PARTNER_LINK:
              return SUBMISSION_WHAKAPAPA_LINK
            case WHAKAPAPA:
              return SUBMISSION_WHAKAPAPA_VIEW
            default: return null
          }
        }
      },
      /**
       * resolver for type SubmissionGroupPerson which implements
       * the Submission interface
       * NOTE: any type which implements the interface will need to add the same
       * things as SubmissionGroupPerson
       */
      SubmissionGroupPerson: {
        ...sharedSubmissionResolvers(),
        source: (submission) => submission.source || 'ahau',
        sourceRecord: (submission) => {
          if (!submission?.sourceId) return
          return getProfileById(submission.sourceId)
        },
        targetRecord: (submission) => {
          if (!submission?.targetId) return
          return p(renderUpdate)(submission.targetId)
          // this shows the update that was performed on the record
          // and the targetRecord.id is the id of the update
        },
        approvedByIds: submission => submission.approvedBy,
        rejectedByIds: submission => submission.rejectedBy,
        joiningQuestions: submission => submission?.moreInfo?.joiningQuestions,
        dependencies: submission => {
          return ssb.submissions.link.list({
            // NOTE: cannot use this filter as it is only applied after the read below
            // filter: (link) => link.parent === submission.key,
            read: (link, cb) => {
              // NOTE: temp fix to a bug where link is a string
              // NOTE: recently changes to ssb-crut are causing issues here in Ahau
              // even though this module doesnt have the new version of it
              const linkId = (typeof link === 'object')
                ? link.key
                : link

              ssb.submissions.link.get(linkId, (err, link) => {
                if (err) return cb(err)

                const { parent, child } = link

                if (parent !== submission.key) return cb(null, null) // to be filtered out
                getSubmission(child, cb)
              })
            }
          })
        }
      },
      SubmissionWhakapapaLink: {
        ...sharedSubmissionResolvers(),
        sourceRecord: submission => getLinkRecord(submission.sourceId, submission.targetType),
        targetRecord: submission => p(renderUpdate)(submission.targetId),
        approvedByIds: submission => submission.approvedBy,
        rejectedByIds: submission => submission.rejectedBy
      },
      SubmissionWhakapapaView: {
        ...sharedSubmissionResolvers(),
        sourceRecord: submission => getWhakapapaRecord(submission.sourceId),
        targetRecord: submission => p(renderUpdate)(submission.targetId),
        approvedByIds: submission => submission.approvedBy,
        rejectedByIds: submission => submission.rejectedBy
      },
      SubmissionComment: {
        author: (comment) => getProfileById(comment.authorId)
      },
      SubmissionLink: {
        id: (link) => link.key,
        mappedDependencies: (link) => convertMapping(link.mappedDependencyFields)
      },
      Person: {
        // NOTE: did this on person, because the altnames returned is not compatible with the
        // person type. Here we just make a new field used specifically for submissions
        // so it doesnt affect person.altNames being used in other places
        altNamesSubmission: (person) => {
          const altNames = person.altNames || { add: [], remove: [] }
          if (Array.isArray(altNames)) return { add: altNames, remove: [] }

          if (!altNames?.add) altNames.add = []
          if (!altNames?.remove) altNames.remove = []

          return altNames
        },
        altNames: (person) => {
          const altNames = person.altNames || []
          if (Array.isArray(altNames)) return altNames

          if (typeof altNames !== 'object') return []
          return altNames?.add || []
        }
      },
      WhakapapaView: {
        // NOTE: did this on whakapapa, because the ignoredProfiles returned is not compatible with the
        // whakapapaView type. Here we just make a new field used specifically for submissions
        // so it doesnt affect whakapapaView.ignoredProfiles being used in other places
        ignoredProfilesSubmission: (whakapapa) => {
          const ignoredProfiles = whakapapa.ignoredProfiles || { add: [], remove: [] }
          if (Array.isArray(ignoredProfiles)) return { add: ignoredProfiles, remove: [] }

          if (!ignoredProfiles?.add) ignoredProfiles.add = []
          if (!ignoredProfiles?.remove) ignoredProfiles.remove = []

          return ignoredProfiles
        },
        ignoredProfiles: (whakapapa) => {
          const ignoredProfiles = whakapapa.ignoredProfiles || []
          if (Array.isArray(ignoredProfiles)) return ignoredProfiles

          if (typeof ignoredProfiles !== 'object') return []
          return ignoredProfiles?.add || []
        }
        // TODO: may need to handle importantRelationships
      }

    },
    gettersWithCache: {}
  }
}

function convertMapping (mapping) {
  if (!mapping) return
  if (typeof mapping !== 'object') return

  if (Array.isArray(mapping)) return convertMappingToObject(mapping)
  return convertMappingToArray(mapping)
}
function convertMappingToObject (mapping) {
  const output = {}
  mapping.forEach(({ missingField, replacementField }) => {
    output[missingField] = replacementField
  })

  return output
}

function convertMappingToArray (mapping) {
  return Object.entries(mapping)
    .map(([missingField, replacementField]) => ({ missingField, replacementField }))
}
