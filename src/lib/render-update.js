// NOTE: we need to do the get a little different here
// because ssb-crut doesnt handle getting a record by

module.exports = function RenderUpdate (ssb) {
  return function renderUpdate (id, cb) {
    if (!id) return cb(null, null)

    ssb.get({ id, private: true }, (err, val) => {
      if (err) return cb(err)

      const crut = ssb.submissions.findHandler({ type: val.content.type })
      if (!crut) return cb(Error('no crut handler registered for type ' + val.content.type))

      const { content } = val
      const output = crut.strategy.mapToOutput(content)

      const result = Object.keys(content).reduce((acc, key) => {
        switch (key) {
          case 'type':
            if (content[key] === 'link/profile-profile/child') {
              acc[key] = content[key]
              acc.linkId = id
            }
            return acc
          case 'tangles':
            return acc
          case 'recps':
          case 'child':
          case 'parent':
            acc[key] = content[key]
            return acc

          case 'altNames':
            acc[key] = Object.entries(content[key]).reduce(
              (acc, [key, value]) => {
                value > 0
                  ? acc.add.push(key)
                  : acc.remove.push(key)
                return acc
              },
              { add: [], remove: [] }
            )
            return acc

          default:
            acc[key] = output[key]
            return acc
        }
      }, { id, key: id, authors: { '*': [{ start: 0, end: null }] } })

      cb(null, result)
    })
  }
}
