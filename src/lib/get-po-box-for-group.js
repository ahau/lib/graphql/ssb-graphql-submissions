const get = require('lodash.get')
const { GraphQLError } = require('graphql')
const { promisify: p } = require('util')

// NOTE: this is an easier version, rather than adding the groupId to the submission and
// using that. If this causes issues in the future, then we can switch it to save the groupId
// instead and use that to resolve the group
module.exports = function GetPoBoxForGroup (ssb) {
  return async function getPoBoxForGroup (groupId) {
    const profiles = await p(ssb.profile.findByGroupId)(groupId)
    let poBoxId = get(profiles, 'public[0].poBoxId')

    // attempt to use ssb-tribes instead to get the poBoxId
    if (!poBoxId) {
      const { poBoxId: _poBoxId } = await p(ssb.tribes.poBox.get)(groupId)
      if (_poBoxId) poBoxId = _poBoxId
      else throw new GraphQLError(`unable to find poBoxId for group ${groupId}`)
    }

    return [poBoxId, ssb.id]
  }
}
