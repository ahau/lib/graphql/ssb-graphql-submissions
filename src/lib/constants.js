const CHILD_LINK = 'link/profile-profile/child'
const PARTNER_LINK = 'link/profile-profile/partner'
const PERSON_ADMIN = 'profile/person/admin'
const PERSON = 'profile/person'
const WHAKAPAPA = 'whakapapa/view'
const SUBMISSION_GROUP_PERSON = 'SubmissionGroupPerson'
const SUBMISSION_WHAKAPAPA_LINK = 'SubmissionWhakapapaLink'
const SUBMISSION_WHAKAPAPA_VIEW = 'SubmissionWhakapapaView'

module.exports = {
  CHILD_LINK,
  PARTNER_LINK,
  PERSON_ADMIN,
  PERSON,
  WHAKAPAPA,
  SUBMISSION_GROUP_PERSON,
  SUBMISSION_WHAKAPAPA_LINK,
  SUBMISSION_WHAKAPAPA_VIEW
}
