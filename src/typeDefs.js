const gql = require('graphql-tag')

module.exports = gql`
  extend type Query {
    getSubmission (id: String!): Submission

    getSubmissions: [Submission]
  
    getSubmissionsLink (
      id: String! # submissionId
    ): SubmissionLink
  }

  extend type Mutation {
    approveSubmission (
      id: String! # submissionId
      comment: String
      targetId: String
    ): String

    approveNewGroupPerson (
      id: String # submissionId
      comment: String
      allowedFields:  PersonProfileInput!
    ): String

    approveEditGroupPerson (
      id: String! # submissionId
      comment: String
      allowedFields: PersonProfileInput!
    ): String

    approveNewWhakapapaLink (
      id: String! # submissionId
      comment: String
      allowedFields: LinkInput! # LinkInput is defined in @ssb-graphql/whakapapa
    ): String

    approveEditWhakapapaLink (
      id: String! # submissionId
      comment: String
      allowedFields: LinkInput! # LinkInput is defined in @ssb-graphql/whakapapa
    ): String

    approveEditWhakapapaView (
      id: String! # submissionId
      comment: String
      allowedFields: WhakapapaViewInput!
    ): String

    rejectSubmission (
      id: String! # submissionId
      comment: String
    ): String

    tombstoneSubmission (
      id: String! # submissionId
      tombstoneInput: TombstoneInput!
    ): String

    proposeNewGroupPerson (
      input: PersonProfileInput!
      comment: String
      groupId: String!
      source: SubmissionSource
      joiningQuestions: [SubmissionJoiningQuestionInput]
    ): String

    proposeEditGroupPerson (
      profileId: String!
      input: PersonProfileInput! # PersonProfileInput is defined in @ssb-graphql/profile
      comment: String
      groupId: String!
    ): String

    proposeNewWhakapapaLink (
      input: LinkInput! # LinkInput is defined in @ssb-graphql/whakapapa
      comment: String
      groupId: String!
    ): String

    proposeEditWhakapapaLink (
      linkId: String!
      input: LinkInput! # LinkInput is defined in @ssb-graphql/whakapapa
      comment: String
      groupId: String!
    ): String

    # TODO: proposeNewWhakapapaView

    proposeEditWhakapapaView (
      whakapapaId: String!
      input: WhakapapaViewInput! # WhakapapaViewInput is defined in @ssb-graphql/whakapapa
      comment: String
      groupId: String
    ): String

    proposeTombstone (
      recordId: String!
      comment: String
      groupId: String!
    ): String

    createSubmissionsLink (
      parent: String
      child: String
      mappedDependencies: [MappedDependencyInput]
    ): String

  }

  input SubmissionJoiningQuestionInput {
    label: String
    value: String
  }

  type SubmissionJoiningQuestion {
    label: String
    value: String
  }

  input MappedDependencyInput {
    missingField: MissingField 
    replacementField: ReplacementField
  }

  enum SubmissionSource {
    ahau
    webForm
  }

  enum MissingField {
    parent
    child
  }

  enum ReplacementField {
    targetId
  }

  type SubmissionComment {
    authorId: String
    author: Person
    comment: String
  }

  interface Submission {
    id: ID!

    groupId: String
    recps: [String]

    comments: [SubmissionComment]

    sourceId: String
    targetId: String
  
    targetType: String

    # Information about the group the submission is in
    group: TribeFaces

    # Information about the person who made the submission
    applicant: Person
    applicantId: String

    # TODO: resolve profiles for these
    approvedByIds: [String]
    rejectedByIds: [String]

    dependencies: [Submission]
  
    tombstone: Tombstone
  }

  type SubmissionGroupPerson implements Submission {
    id: ID!

    groupId: String
    recps: [String]

    details: Person
    comments: [SubmissionComment]

    sourceId: String
    targetId: String
    targetType: String
    sourceRecord: Person
    targetRecord: Person

    # Information about the group the submission is in
    group: TribeFaces

    # Information about the person who made the submission
    applicant: Person
    applicantId: String

    # Information about where the submission came from
    source: SubmissionSource

    # TODO: resolve profiles for these
    approvedByIds: [String]
    rejectedByIds: [String]

    # Additional information included in the submission
    joiningQuestions: [SubmissionJoiningQuestion]

    dependencies: [Submission]
  
    tombstone: Tombstone
  }

  extend type Person {
    altNamesSubmission: SubmissionDetailsSet
  }

  type SubmissionDetailsSet {
    add: [String]
    remove: [String]
  }

  type SubmissionWhakapapaLink implements Submission {
    id: ID!

    groupId: String
    recps: [String]

    targetType: String

    details: WhakapapaLink
    comments: [SubmissionComment]

    # source is an existing record which we propose(d) updates to
    sourceId: String
    sourceRecord: WhakapapaLink
  
    # target is the new record which holds the results of the proposal
    targetId: String
    targetRecord: WhakapapaLink

    # Information about the group the submission is in
    group: TribeFaces

    # Information about the person who made the submission
    applicant: Person
    applicantId: String

    # TODO: resolve profiles for these
    approvedByIds: [String]
    rejectedByIds: [String]

    dependencies: [Submission]
  
    tombstone: Tombstone
  }

  type SubmissionWhakapapaView implements Submission {
    id: ID!

    groupId: String
    recps: [String]

    targetType: String

    details: WhakapapaView
    comments: [SubmissionComment]

    # source is an existing record which we propose(d) updates to
    sourceId: String
    sourceRecord: WhakapapaView
  
    # target is the new record which holds the results of the proposal
    targetId: String
    targetRecord: WhakapapaView

    # Information about the group the submission is in
    group: TribeFaces

    # Information about the person who made the submission
    applicant: Person
    applicantId: String

    # TODO: resolve profiles for these
    approvedByIds: [String]
    rejectedByIds: [String]

    dependencies: [Submission]
  
    tombstone: Tombstone
  }

  extend type WhakapapaView {
    ignoredProfilesSubmission: SubmissionDetailsSet
  }

  type SubmissionLink {
    id: ID!

    parent: String
    child: String

    mappedDependencies: [MappedDependency]
  }

  type MappedDependency {
    missingField: MissingField 
    replacementField: ReplacementField
  }
  
`
